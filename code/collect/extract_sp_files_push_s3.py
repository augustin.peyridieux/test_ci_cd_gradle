import sys
sys.path.append("utils/")
sys.path.append("../dependencies/utils/")
sys.path.append("pricing_project_industry-master/code/dependencies/utils/")
from sharepoint import SharepointApi
import utils
import os
import boto3
import argparse
from datetime import datetime, timezone
import pytz

logger = utils.create_logger('PPI: transfer SP to S3')

logger.info("Starting: " + str(sys.argv[0]))

desc = "Pricing Project Industry: Read Sharepoint files and push them to AWS S3"
parser = argparse.ArgumentParser(description=desc)
parser.add_argument("sharepoint_folder",
                    help="Sharepoint folder to get files from")
parser.add_argument("bucket_name",
                    help="Name of the AWS S3 bucket name")
parser.add_argument("aws_folder",
                    help=("Name of the AWS S3 folder"))
parser.add_argument("aws_folder_intermediate",
                    help=("Name of the AWS S3 folder"))
args = parser.parse_args()

args.sharepoint_folder = utils.add_backslash(args.sharepoint_folder)
args.aws_folder = utils.add_backslash(args.aws_folder)
args.aws_folder_intermediate = utils.add_backslash(args.aws_folder_intermediate)


logger.info("sharepoint_folder: " + str(args.sharepoint_folder))
logger.info("bucket_name: " + str(args.bucket_name))
logger.info("aws_folder: " + str(args.aws_folder))
logger.info("aws_folder_intermediate: " + str(args.aws_folder_intermediate))


# Déclaration des variables sharepoint
# Nom de domaine
host = os.environ['SHAREPOINT_HOST']
# Nom de la team / site / autre où les fichiers ou autres seront stockés (visible dans l'url quand dans le dossier sharepoint)
site = os.environ['SP_PPI_SITE']
# Client ID à demander à l'administrateur Office365
client_id = os.environ['PPI_CLIENT_ID']
# Client secret à demander à l'administrateur Office365
client_secret = os.environ['PPI_SP_SECRET']
# Tenant id à demander à l'administrateur Office365
tenant_id = os.environ['PPI_SP_TENANT_ID']

sharepoint_access_url = os.environ['SHAREPOINT_ACCESS_URL']

logger.info("host: " + host)
logger.info("site: " + site)
logger.info("client_id: " + client_id)
logger.info("sharepoint_access_url: " + sharepoint_access_url)

logger.info("Initialize Sharepoint client")
my_sharepointAPI = SharepointApi(host, site, client_secret, client_id, tenant_id, url_access=sharepoint_access_url)

logger.info("Initialize S3 client")

s3_resource = boto3.resource('s3')
my_bucket = s3_resource.Bucket(name=args.bucket_name)

version = utils.get_verision_data(my_bucket, args, logger, True)

list_files = my_sharepointAPI.get_list_files_from_folder(path_folder=args.sharepoint_folder)['value']

logger.info("Number of files: " + str(len(list_files)))

logger.info("Archiving existing files")
folder_backup = datetime.now(pytz.timezone('Europe/Amsterdam')).strftime("%d%m%Y_%H%M") + '/'

for file in utils.get_list_aws_file_in_directory(args.aws_folder, my_bucket):
    # test if file is not in a subdirectory
    if len(file.split('/')) == len(args.aws_folder.split('/')):
        file_dest = args.aws_folder + folder_backup + file.split('/')[-1]
        utils.move_aws_file(file, file_dest, my_bucket, logger)

for file in list_files:
    file_name = file['Name']

    logger.info("Download from Sharepoint file : " + file_name)
    my_sharepointAPI.downloadFileFromSharepoint(foldername=args.sharepoint_folder, filename=file_name)

    logger.info("Upload to AWS S3 file: " + file_name)
    s3_resource.meta.client.upload_file(file['Name'], args.bucket_name,
                                        args.aws_folder + file['Name'])


