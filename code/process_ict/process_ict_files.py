import sys
sys.path.append("utils/")
sys.path.append("../dependencies/utils/")
sys.path.append("pricing_project_industry-master/code/dependencies/utils/")
import utils
from utils_ict import read_ict_round_files, read_ict_square_files, get_df_ict_from_file, get_parameters
import boto3
import argparse


logger = utils.create_logger('PPI: Reading ICT files')

logger.info("Starting: " + str(sys.argv[0]))

args = get_parameters(logger)

# Getting S3 client
logger.info("Getting S3 client")

s3_resource = boto3.resource('s3')
my_bucket = s3_resource.Bucket(name=args.bucket_name)

logger.info("Download AWS file: " + args.aws_folder_raw + args.ict_round_file)
my_bucket.download_file(args.aws_folder_raw + args.ict_round_file, args.ict_round_file)

logger.info("Download AWS file: " + args.aws_folder_raw + args.ict_square_file)
my_bucket.download_file(args.aws_folder_raw + args.ict_square_file, args.ict_square_file)

df_ict_round = get_df_ict_from_file(args.ict_round_file, "round", logger)
df_ict_square = get_df_ict_from_file(args.ict_square_file, "square", logger)

#sanitize Company_Group column
df_ict_round = utils.sanitize_str_df(df_ict_round, df_ict_round.select_dtypes(include='object').columns, logger)
df_ict_square = utils.sanitize_str_df(df_ict_square, df_ict_square.select_dtypes(include='object').columns, logger)

df_ict_round.to_csv(args.ict_round_file_out, index=False)
df_ict_square.to_csv(args.ict_square_file_out, index=False)

logger.info("Upload to AWS S3 file: " + args.aws_folder_intermediate + args.ict_round_file_out)
s3_resource.meta.client.upload_file(args.ict_round_file_out, args.bucket_name,
                                    args.aws_folder_intermediate + args.ict_round_file_out)

logger.info("Upload to AWS S3 file: " + args.aws_folder_intermediate + args.ict_square_file_out)
s3_resource.meta.client.upload_file(args.ict_square_file_out, args.bucket_name,
                                    args.aws_folder_intermediate + args.ict_square_file_out)
