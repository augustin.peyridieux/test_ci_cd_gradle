import sys
sys.path.append("utils/")
sys.path.append("../dependencies/utils/")
sys.path.append("pricing_project_industry-master/code/dependencies/utils/")
import utils
import common_business_rules as cbr
import pandas as pd
import argparse


def get_parameters(logger):
    desc = "Pricing Project Industry: Read Sharepoint files and push them to AWS S3"
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("bucket_name",
                        help="Name of the AWS S3 bucket name")
    parser.add_argument("aws_folder_raw",
                        help=("Name of the AWS S3 folder raw"))
    parser.add_argument("aws_folder_intermediate",
                        help=("Name of the AWS S3 folder intermediate"))
    parser.add_argument("ict_round_file",
                        help=("Name of the ICT round file"))
    parser.add_argument("ict_square_file",
                        help=("Name of the ICT square file"))
    parser.add_argument("ict_round_file_out",
                        help=("Name of the ICT round file ouput"))
    parser.add_argument("ict_square_file_out",
                        help=("Name of the ICT square file output"))
    args = parser.parse_args()

    args.aws_folder_raw = utils.add_backslash(args.aws_folder_raw)
    args.aws_folder_intermediate = utils.add_backslash(args.aws_folder_intermediate)

    logger.info("bucket_name: " + str(args.bucket_name))
    logger.info("aws_folder_raw: " + str(args.aws_folder_raw))
    logger.info("aws_folder_intermediate: " + str(args.aws_folder_intermediate))
    logger.info("ict_round_file: " + str(args.ict_round_file))
    logger.info("ict_square_file: " + str(args.ict_square_file))
    logger.info("ict_round_file_out: " + str(args.ict_round_file_out))
    logger.info("ict_square_file_out: " + str(args.ict_square_file_out))
    return args


# Permet de lire un onglet du fichier Excel
def read_ict_square_files(file_name, sheet_name, j, logger):
    logger.info(file_name + ": " + sheet_name)
    df = pd.read_excel(file_name, sheet_name, header= None)
    df1 = df.iloc[17:].reset_index(drop = True)
    df2 = df1.loc[:,[0,1]]
    df2.drop([0,1], axis = 0, inplace = True)
    df2 = df2.reset_index(drop = True)
    df2 = df2.rename(columns={0: 'Height', 1: 'Width'})
    df2['Height'] = df2['Height'].apply(lambda x: float(str(x).replace(' ', '').replace(',', '.')))
    df2['Width'] = df2['Width'].apply(lambda x: float(str(x).replace(' ', '').replace(',', '.')))
    df2['max_length(mm)'] = [str(round(df2['Height'].iloc[x],3)) + ' / ' + str(round(df2['Width'].iloc[x], 3)) for x in df2.index]
    d = df1.columns[2:]
    df3 = df1.loc[:, d]
    df3 = df3.iloc[[0]]
    df3_t = df3.T
    df3_t = df3_t.rename(columns={0: 'AWT(mm)'})
    df3_t = df3_t.reset_index(drop = True)#.dropna(how='all')
    df3_t['AWT(mm)'] = [str(round(df3_t['AWT(mm)'].iloc[x],3)) for x in df3_t.index]
    df1.drop([0,1], axis = 0, inplace = True)
    df1.drop([0], axis = 1, inplace = True)
    df1 = df1.reset_index(drop = True)
    df1[1] = df2['max_length(mm)']
    df1 = df1.rename(columns={1: 'max_length(mm)'})
    old = df1.columns
    new = df3_t['AWT(mm)'].values
    for i in range(1, len(old)):
        df1 = df1.rename(columns={old[i] : new[i-1]})
    col = df1.columns[1:]
    val = pd.melt(df1, id_vars =['max_length(mm)'], value_vars = col)
    val = val.rename(columns={'variable':'AWT(mm)', 'value':'Length'})
    df4 = df.iloc[0:16]
    a0 = df4.iloc[0]
    a0 = [x for x in a0 if str(x) != 'nan' and str(x)!= 'Company Group']
    a0 = a0 if len(a0) > 0 else [None]
    a1 = df4.iloc[1]
    a1 = [x for x in a1 if str(x) != 'nan' and str(x)!= 'Mill Name']
    a1 = a1 if len(a1) > 0 else [None]
    a2 = df4.iloc[2]
    a2 = [x for x in a2 if str(x) != 'nan' and str(x)!= 'VLR Region']
    a2 = a2 if len(a2) > 0 else [None]
    a3 = df4.iloc[3]
    a3 = [x for x in a3 if str(x) != 'nan' and str(x)!= 'Country']
    a3 = a3 if len(a3) > 0 else [None]
    a4 = df4.iloc[4]
    a4 = [x for x in a4 if str(x) != 'nan' and str(x)!= 'Manufacturing process list']
    a4 = a4 if len(a4) > 0 else[None]
    a5 = df4.iloc[5]
    a5 = [x for x in a5 if str(x) != 'nan' and (str(x)!= 'Total capacity in t/year' and str(x)!= 'Total capacity in kt/year')]
    a5 = a5 if len(a5) > 0 else [None]
    a6 = df4.iloc[6]
    a6 = [x for x in a6 if str(x) != 'nan' and str(x)!= 'Qualification list']
    a6 = a6 if len(a6) > 0 else [None]
    a7 = df4.iloc[7]
    a7 = [x for x in a7 if str(x) != 'nan' and str(x)!= 'Theoretical max length in m']
    a7 = a7 if len(a7) > 0 else [None]
    a8 = df4.iloc[8]
    a8 = [x for x in a8 if str(x) != 'nan' and str(x)!= 'OD tolerance in %']
    a8 = a8 if len(a8) > 0 else [None]
    a9 = df4.iloc[9]
    a9 = [x for x in a9 if str(x) != 'nan' and str(x)!= 'WT tolerance in %']
    a9 = a9 if len(a9) > 0 else [None]
    a10 = df4.iloc[10]
    a10 = [x for x in a10 if str(x) != 'nan' and (str(x)!= 'Comment: ' and str(x)!= 'comment')]
    a10 = a10 if len(a10) > 0 else [None]
    a11 = df4.iloc[11]
    a11 = [x for x in a11 if str(x) != 'nan' and str(x)!= 'source date']
    a11 = a11 if len(a11) > 0 else [None]
    a12 = df4.iloc[12]
    a12 = [x for x in a12 if str(x) != 'nan' and str(x)!= 'update date']
    a12 = a12 if len(a12) > 0 else [None]
    a14 = df4.iloc[14]
    a14 = [x for x in a14 if str(x) != 'nan' and str(x)!= 'grade classes of products']
    a14 = a14 if len(a14) > 0 else [None]
    a15 = df4.iloc[15]
    a15 = [x for x in a15 if str(x) != 'nan' and (str(x)!= 'highest grade' and str(x)!='grade sub-class')]
    a15 = a15 if len(a15) > 0 else [None]
    df_grade = pd.DataFrame(a14, columns=['grade_classes_of_products'])
    val = pd.merge(val.assign(key='x'), df_grade.assign(key='x'), on='key').drop('key', 1)
    a0 = pd.Series(a0, name = 'Company_Group')
    val = val.join(a0)
    a1 = pd.Series(a1, name = 'Mill_Name')
    val = val.join(a1)
    a2 = pd.Series(a2, name = 'VLR_Region')
    val = val.join(a2)
    a3 = pd.Series(a3, name = 'Country')
    val = val.join(a3)
    a4 = pd.Series(a4, name = 'Manufacturing_process_list')
    val = val.join(a4)
    a5 = pd.Series(a5, name = 'Total_capacity_in_t/year')
    val = val.join(a5)
    a6 = pd.Series(a6, name = 'Qualification_list')
    val = val.join(a6)
    a7 = pd.Series(a7, name = 'Theoretical_max_length_in_m')
    val = val.join(a7)
    a8 = pd.Series(a8, name = 'OD_tolerance_in_%')
    val = val.join(a8)
    a9 = pd.Series(a9, name = 'WT_tolerance_in_%')
    val = val.join(a9)
    a10 = pd.Series(a10, name = 'comment')
    val = val.join(a10)
    a11 = pd.Series(a11, name = 'source_date')
    val = val.join(a11)
    a12 = pd.Series(a12, name = 'update_date')
    val = val.join(a12)
    a15 = pd.Series(a15, name = 'highest_grade')
    val = val.join(a15)
    val[['Company_Group', 'Mill_Name', 'VLR_Region', 'Country', 'Manufacturing_process_list','Total_capacity_in_t/year','Qualification_list','Theoretical_max_length_in_m','OD_tolerance_in_%','WT_tolerance_in_%','comment','source_date','update_date','highest_grade']] =[val['Company_Group'][0], val['Mill_Name'][0], val['VLR_Region'][0], val['Country'][0], ','.join(str(x) for x in val['Manufacturing_process_list'].dropna()), val['Total_capacity_in_t/year'][0],','.join(str(x) for x in val['Qualification_list'].dropna()),val['Theoretical_max_length_in_m'][0],val['OD_tolerance_in_%'][0],val['WT_tolerance_in_%'][0],val['comment'][0],val['source_date'][0],val['update_date'][0],val['highest_grade'][0]]
    val['Height'], val['Width']= val['max_length(mm)'].str.split('/', 1).str
    val.drop(['max_length(mm)'], axis = 1, inplace = True)
    val = val.dropna(subset = ['Length'])
    val = val.reset_index(drop= True)
    val['AWT(mm)'] = val['AWT(mm)'].apply(lambda x: x if str(x).replace(' ', '') != 'nan' else None)
    val['Height'] = val['Height'].apply(lambda x: x if str(x).replace(' ', '') != 'nan' else None)
    val['Width'] = val['Width'].apply(lambda x: x if str(x).replace(' ', '') != 'nan' else None)
    val[['AWT(mm)', 'Height', 'Width']] = val[['AWT(mm)', 'Height', 'Width']].apply(pd.to_numeric)
    val['ID'] = j
    val['ID'] = ['SQ'+ str(val['ID'].iloc[x]) for x in val.index ]
    return val


def read_ict_round_files(file_name, sheet_name, j, logger):
    logger.info(file_name + ": " + sheet_name)
    df = pd.read_excel(file_name, sheet_name, header= None)
    df1 = df.iloc[17:].reset_index(drop = True)
    df2 = df1.loc[:,[0,1]]
    df2.drop([0,1], axis = 0, inplace = True)
    df2 = df2.reset_index(drop = True)
    df2 = df2.rename(columns={0: 'OD(mm)', 1: 'OD(in)'})
    df2['OD(mm)'] = df2['OD(mm)'].apply(lambda x: float(str(x).replace(' ', '').replace(',', '.')))
    df2['OD(in)'] = df2['OD(in)'].apply(lambda x: float(str(x).replace(' ', '').replace(',', '.')))
    df2['max length (mm)'] = [str(round(df2['OD(mm)'].iloc[x],3)) + ' / ' + str(round(df2['OD(in)'].iloc[x], 3)) for x in df2.index]
    d = df1.columns[2:]
    df3 = df1.loc[:, d]
    df3 = df3.iloc[[0, 1]]
    df3_t = df3.T
    df3_t = df3_t.rename(columns={0: 'AWT(mm)', 1: 'AWT(in)'})
    df3_t = df3_t.reset_index(drop = True)#.dropna(how='all')
    df3_t['AWT(mm/in)'] = [str(round(float(df3_t['AWT(mm)'].iloc[x]),3)) + ' / ' + str(round(float(df3_t['AWT(in)'].iloc[x]), 3)) for x in df3_t.index]
    df1.drop([0,1], axis = 0, inplace = True)
    df1.drop([1], axis = 1, inplace = True)
    df1 = df1.reset_index(drop = True)
    df1[0] = df2['max length (mm)']
    old = df1.columns
    new = df3_t['AWT(mm/in)'].values
    for i in range(1, len(old)):
        df1 = df1.rename(columns={old[i] : new[i-1]})
    col = df1.columns[1:]
    val = pd.melt(df1, id_vars =[0], value_vars = col)
    val = val.rename(columns={0 : 'Max_length(mm)', 'variable':'AWT(mm/in)', 'value':'Length'})
    df4 = df.iloc[0:16]
    a0 = df4.iloc[0]
    a0 = [x for x in a0 if str(x) != 'nan' and str(x)!= 'Company Group']
    a0 = a0 if len(a0) > 0 else [None]
    a1 = df4.iloc[1]
    a1 = [x for x in a1 if str(x) != 'nan' and str(x)!= 'Mill Name']
    a1 = a1 if len(a1) > 0 else [None]
    a2 = df4.iloc[2]
    a2 = [x for x in a2 if str(x) != 'nan' and str(x)!= 'VLR Region']
    a2 = a2 if len(a2) > 0 else [None]
    a3 = df4.iloc[3]
    a3 = [x for x in a3 if str(x) != 'nan' and str(x)!= 'Country']
    a3 = a3 if len(a3) > 0 else [None]
    a4 = df4.iloc[4]
    a4 = [x for x in a4 if str(x) != 'nan' and str(x)!= 'Manufacturing process list']
    a4 = a4 if len(a4) > 0 else [None]
    a5 = df4.iloc[5]
    a5 = [x for x in a5 if str(x) != 'nan' and str(x)!= 'Total capacity in t/year']
    a5 = a5 if len(a5) > 0 else [None]
    a6 = df4.iloc[6]
    a6 = [x for x in a6 if str(x) != 'nan' and str(x)!= 'Qualification list']
    a6 = a6 if len(a6) > 0 else [None]
    a7 = df4.iloc[7]
    a7 = [x for x in a7 if str(x) != 'nan' and str(x)!= 'Theoretical max length in m']
    a7 = a7 if len(a7) > 0 else [None]
    a8 = df4.iloc[8]
    a8 = [x for x in a8 if str(x) != 'nan' and str(x)!= 'OD tolerance in %']
    a8 = a8 if len(a8) > 0 else [None]
    a9 = df4.iloc[9]
    a9 = [x for x in a9 if str(x) != 'nan' and str(x)!= 'WT tolerance in %']
    a9 = a9 if len(a9) > 0 else [None]
    a10 = df4.iloc[10]
    a10 = [x for x in a10 if str(x) != 'nan' and (str(x)!= 'Comment: ' and str(x)!= 'comment')]
    a10 = a10 if len(a10) > 0 else [None]
    a11 = df4.iloc[11]
    a11 = [x for x in a11 if str(x) != 'nan' and str(x)!= 'source date']
    a11 = a11 if len(a11) > 0 else [None]
    a12 = df4.iloc[12]
    a12 = [x for x in a12 if str(x) != 'nan' and str(x)!= 'update date']
    a12 = a12 if len(a12) > 0 else [None]
    a14 = df4.iloc[14]
    a14 = [x for x in a14 if str(x) != 'nan' and str(x)!= 'grade classes of products']
    a14 = a14 if len(a14) > 0 else [None]
    a15 = df4.iloc[15]
    a15 = [x for x in a15 if str(x) != 'nan' and (str(x)!= 'highest grade' and str(x)!='grade sub-class')]
    a15 = a15 if len(a15) > 0 else [None]
    df_grade = pd.DataFrame(a14, columns=['grade_classes_of_products'])
    val = pd.merge(val.assign(key='x'), df_grade.assign(key='x'), on='key').drop('key', 1)
    a0 = pd.Series(a0, name = 'Company_Group')
    val = val.join(a0)
    a1 = pd.Series(a1, name = 'Mill_Name')
    val = val.join(a1)
    a2 = pd.Series(a2, name = 'VLR_Region')
    val = val.join(a2)
    a3 = pd.Series(a3, name = 'Country')
    val = val.join(a3)
    a4 = pd.Series(a4, name = 'Manufacturing_process_list')
    val = val.join(a4)
    a5 = pd.Series(a5, name = 'Total_capacity_in_t/year')
    val = val.join(a5)
    a6 = pd.Series(a6, name = 'Qualification_list')
    val = val.join(a6)
    a7 = pd.Series(a7, name = 'Theoretical_max_length_in_m')
    val = val.join(a7)
    a8 = pd.Series(a8, name = 'OD_tolerance_in_%')
    val = val.join(a8)
    a9 = pd.Series(a9, name = 'WT_tolerance_in_%')
    val = val.join(a9)
    a10 = pd.Series(a10, name = 'comment')
    val = val.join(a10)
    a11 = pd.Series(a11, name = 'source_date')
    val = val.join(a11)
    a12 = pd.Series(a12, name = 'update_date')
    val = val.join(a12)
    a15 = pd.Series(a15, name = 'highest_grade')
    val = val.join(a15)
    val[['Company_Group', 'Mill_Name', 'VLR_Region', 'Country', 'Manufacturing_process_list','Total_capacity_in_t/year','Qualification_list','Theoretical_max_length_in_m','OD_tolerance_in_%','WT_tolerance_in_%','comment','source_date','update_date','highest_grade']] =[val['Company_Group'][0], val['Mill_Name'][0], val['VLR_Region'][0], val['Country'][0], ','.join(str(x) for x in val['Manufacturing_process_list'].dropna()), val['Total_capacity_in_t/year'][0],','.join(str(x) for x in val['Qualification_list'].dropna()),val['Theoretical_max_length_in_m'][0],val['OD_tolerance_in_%'][0],val['WT_tolerance_in_%'][0],val['comment'][0],val['source_date'][0],val['update_date'][0],val['highest_grade'][0]]
    val['OD(mm)'], val['OD(in)']= val['Max_length(mm)'].str.split('/', 1).str
    val['AWT(mm)'], val['AWT(in)']= val['AWT(mm/in)'].str.split('/', 1).str
    val.drop(['Max_length(mm)','AWT(mm/in)' ], axis = 1, inplace = True)
    val = val.dropna(subset = ['Length'])
    val = val.reset_index(drop= True)
    val['OD(mm)'] = val['OD(mm)'].apply(lambda x: x if str(x).replace(' ', '') != 'nan' else None)
    val['OD(in)'] = val['OD(in)'].apply(lambda x: x if str(x).replace(' ', '') != 'nan' else None)
    val['AWT(mm)'] = val['AWT(mm)'].apply(lambda x: x if str(x).replace(' ', '') != 'nan' else None)
    val['AWT(in)'] = val['AWT(in)'].apply(lambda x: x if str(x).replace(' ', '') != 'nan' else None)
    val[['OD(mm)','OD(in)', 'AWT(mm)', 'AWT(in)']] = val[['OD(mm)','OD(in)', 'AWT(mm)', 'AWT(in)']].apply(pd.to_numeric)
    val['ID'] = j
    val['ID'] = ['SD'+ str(val['ID'].iloc[x]) for x in val.index ]
    return val


def get_df_ict_from_file(file_name, file_type, logger):
    logger.info("get df for file '" + file_name + "' of type '" + file_type + "'")
    ict_excel = pd.ExcelFile(file_name)

    sheet_names = ict_excel.sheet_names[3:]

    if file_type == "square":
        df_ict = pd.concat(
            [read_ict_square_files(file_name, sheet_names[j], j, logger) for j in range(0, len(sheet_names))])
    elif file_type == "round":
        df_ict = pd.concat(
            [read_ict_round_files(file_name, sheet_names[j], j, logger) for j in range(0, len(sheet_names))])
    else:
        raise NameError("Wrong value for parameter 'file_type'. Either 'square' or 'round'.")
    return df_ict