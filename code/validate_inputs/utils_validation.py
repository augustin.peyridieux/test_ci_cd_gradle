import sys
sys.path.append("utils/")
sys.path.append("../dependencies/utils/")
sys.path.append("pricing_project_industry-master/code/dependencies/utils/")
import utils
import pandas as pd
import argparse

def get_parameters(logger):
    desc = "Pricing Project Industry: Read Sharepoint files and push them to AWS S3"
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("bucket_name",
                        help="Name of the AWS S3 bucket name")
    parser.add_argument("aws_folder_raw",
                        help=("Name of the AWS S3 folder raw"))
    parser.add_argument("aws_folder_intermediate",
                        help=("Name of the AWS S3 folder intermediate"))
    parser.add_argument("grouping_tech_var_file",
                        help=("Name of the technical variable mapping file"))
    parser.add_argument("map_enduser_file",
                        help=("Name of the enduser mapping file"))
    parser.add_argument("map_orderer_file",
                         help=("Name of the orderer mapping file"))
    parser.add_argument("grade_competitor_file",
                        help=("Grade competitor mapping file"))
    parser.add_argument("od_wt_ref_file",
                        help=("Name of the OD WT referential file"))
    parser.add_argument("map_marketing_segment_file",
                        help=("Name of the marketing segment file"))
    parser.add_argument("map_mill_ict_yellow_file",
                        help=("Name of the mapping Millsi ICT YellowFin file"))
    parser.add_argument("map_mill_grade_file",
                        help=("Name of the mapping Mill Grade file"))
    parser.add_argument("list_competitor_file",
                        help=("Name of the list of relevant file"))
    args = parser.parse_args()

    args.aws_folder_raw = utils.add_backslash(args.aws_folder_raw)
    args.aws_folder_intermediate = utils.add_backslash(args.aws_folder_intermediate)

    logger.info("bucket_name: " + str(args.bucket_name))
    logger.info("aws_folder_raw: " + str(args.aws_folder_raw))
    logger.info("aws_folder_intermediate: " + str(args.aws_folder_intermediate))
    logger.info("grouping_tech_var_file: " + str(args.grouping_tech_var_file))
    logger.info("map_enduser_file: " + str(args.map_enduser_file))
    logger.info("map_orderer_file: " + str(args.map_orderer_file))
    logger.info("grade_competitor_file: " + str(args.grade_competitor_file))
    logger.info("od_wt_ref_file: " + str(args.od_wt_ref_file))
    logger.info("map_marketing_segment_file: " + str(args.map_marketing_segment_file))
    logger.info("map_mill_ict_yellow_file: " + str(args.map_mill_ict_yellow_file))
    logger.info("map_mill_grade_file: " + str(args.map_mill_grade_file))
    logger.info("list_competitor_file: " + str(args.list_competitor_file))

    return args


def check_grouping_tech_var_file(dico, logger):

    # Check some values length
    validation_status = validate(len(dico['yfb_length_type'].values()), 3, "yfb_length_type length", logger)
    validation_status = validation_status & validate(len(dico['yfb_grade_3'].values()), 2, "yfb_grade_3 length", logger)

    # Check some mappings
    validation_status = validation_status & validate(dico['yfb_length_type']['LONGUEUR COURANTE'], 'RANDOM LENGTH',
                                                     "Check yfb_length_type value mapping", logger)
    validation_status = validation_status & validate(dico['yfb_heat_treatment'][
        'QUENCHED AND TEMPERED AND AFTER COLD ROTARY STRAIGHTENING STRESS RELIEVE'], 'QUENCHED AND TEMPERED',
                                                     "Check yfb_heat_treatment value (1) mapping", logger)
    validation_status = validation_status & validate(dico['yfb_heat_treatment']['TREMPE AIR + REVENU'],
                                                     'AIR QUENCH + TEMPERING',
                                                     "Check yfb_heat_treatment value (2) mapping", logger)
    validation_status = validation_status & validate(dico['yfb_ultrasound_type'][
        'UT AND (FLUX LEAKAGE OR EDDY CURRENT OR HYDROTEST, AT MILL CHOICE)'],
                                                     'UT OR FLUX LEAKAGE OR EDDY CURRENT OR HYDROTEST, AT MILL CHOICE',
                                                     "Check yfb_ultrasound_type value mapping", logger)
    validation_status = validation_status & validate(dico['yfb_curvature_tolerance']['CURVATURE TO THE INSIDE <= 0,4 MM'],
                                                     'CONCAVITY AND CONVEXITY : TO THE INSIDE <= 0,4 MM',
                                                     "Check yfb_curvature_tolerance value mapping", logger)
    validation_status = validation_status & validate(dico['yfb_ext_corner_radius'][
        'EXTERNAL CORNER PROFILE : OUTER EDGE RADIUS (SIGHT EDGE C) RA: MAX.  1.5 X'],
                                                     'EXTERNAL CORNER PROFILE : OUTER EDGE RADIUS (SIGHT EDGE C) RA: MAX.  1,5 X',
                                                     "Check yfb_ext_corner_radius value mapping", logger)
    validation_status = validation_status & validate(dico['yfb_norm_1'][
        'API 5L, PSL 2, APRIL 2018 REV. 46 (E1),  EXCEPT SPECIFIC ORDER AMENDMENT('], 'API 5L, PSL 2, REV. 46, 2018',
                                                     "Check yfb_norm_1 value mapping", logger)
    validation_status = validation_status & validate(dico['yfb_norm_2'][
        'ISO 3183, PSL 2, 2012 AMD 1,2017, EXCEPT SPECIFIC ORDER AMENDMENT(S)'], 'ISO 3183, PSL 2, 2012 AMD 1,2017',
                                                     "Check yfb_norm_2 value mapping", logger)
    validation_status = validation_status & validate(dico['yfb_norm_3']['EN 10225, 2009,  EXCEPT SPECIFIC ORDER AMENDMENT(S)'],
                                                     'EN 10225, 2009',
                                                     "Check yfb_norm_3 value mapping", logger)
    validation_status = validation_status & validate(dico['yfb_grade_1']['E470 + AR, EN 10297-1, JUNE 2003'],
                                                     'E470 + AR, EN 10297-1, 2003',
                                                     "Check yfb_grade_1 value mapping", logger)
    validation_status = validation_status & validate(dico['yfb_grade_2']['20MNV6 V, MAT.SHEET 039 R, JULY 2004'],
                                                     '20MNV6K + V, MAT.SHEET 039 R',
                                                     "Check yfb_grade_2 value mapping", logger)
    validation_status = validation_status & validate(dico['yfb_grade_3']['S355G15+ N, EN 10225'],
                                                     'S355G15+N, EN 10225',
                                                     "Check yfb_grade_3 value mapping", logger)

    # Check merged cells
    validation_status = validation_status & validate(list(dico['yfb_norm_1'].values())
                                                     .count('API 5L, PSL 2, REV. 46, 2018'),
                                                     6,
                                                     "Count yfb_norm_1 value occurences", logger)
    validation_status = validation_status & validate(list(dico['yfb_norm_2'].values())
                                                     .count('ISO 3183, PSL 2, 2019'),
                                                     2,
                                                     "Count yfb_norm_2 value occurences", logger)
    validation_status = validation_status & validate(list(dico['yfb_norm_3'].values())
                                                     .count('EN 10216-2, TC1, 2013'),
                                                     2,
                                                     "Count yfb_norm_3 value occurences", logger)
    validation_status = validation_status & validate(list(dico['yfb_grade_3'].values())
                                                     .count('S355G15+N, EN 10225'),
                                                     2,
                                                     "Count yfb_grade_3 value occurences", logger)
    validation_status = validation_status & validate(list(dico['yfb_grade_2'].values())
                                                     .count('X65 Q, API 5L / ISO 3183, PSL 2'),
                                                     3,
                                                     "Count yfb_grade_2 value occurences", logger)
    validation_status = validation_status & validate(list(dico['yfb_grade_1'].values())
                                                     .count('AVADURÂ® 758(110), MAT.SHEET AVA-01, REV. 0, FEBRUARY 13-2012'),
                                                     2,
                                                     "Count yfb_grade_1 value occurences", logger)

    return validation_status & validate(len(dico.keys()), 11, "Number of keys in the output dictionary", logger)


def check_map_enduser_file(df, logger):
    validation_status = validate(len(df['Enduser Text 1 (B)'].unique()),
                                 len(df['Enduser Text 1 (B)']),
                                 "Enduser Text 1 (B) duplication", logger)
    validation_status = validation_status & validate(len(df['Enduser Group (refers to Enduser Text)']),
                                                     len(df['Enduser Text 1 (B)']),
                                                     "Enduser Group duplication", logger)

    return validation_status


def check_map_orderer_file(df, logger):
    validation_status = validate(len(df['Orderer'].unique()),
                                 len(df['Orderer']), "Orderer duplication", logger)
    validation_status = validation_status & validate(len(df['Orderer Group (refers to Orderer)']),
                                 len(df['Orderer']), "length Order Group equals to Orderer", logger)
    validation_status = validation_status & validate(len(df['Orderer Type (refers to Orderer)'].unique()),
                                                     4,
                                                     "Orderer type length", logger)

    return validation_status


def check_grade_competitor_file(df, logger):
    validation_status = validate(len(df['Competitors_grades'].unique()),
                                 len(df['Competitors_grades']),
                                 "Competitors grades duplication", logger)
    return validation_status

def check_od_wt_ref_file(df, logger):
    validation_status = validate(df.isna().sum()['od_mm'],
                                 0,
                                 "OD mm missing values", logger)
    validation_status = validation_status & validate(df.isna().sum()['wt_mm'],
                                                     0,
                                                     "WT mm missing values", logger)

    return validation_status


def check_map_marketing_segment_file(df, logger):
    validation_status = validate(len(df['Enduser Text 1 (B)']),
                                 len(df['Enduser Text 1 (B)'].unique()),
                                 "Enduser duplication", logger)
    validation_status = validation_status & validate(len(df['Enduser Text 1 (B)']),
                                                     len(df['Marketing Segment (refers to Enduser Text)']),
                                                     "Marketing Segment missing values", logger)
    return validation_status


def check_map_mill_ict_yellow_file(df, logger):
    validation_status = validate(len(df['ICT Mill Names']),
                                 len(df['ICT Mill Names'].unique()),
                                 "Mill Names duplication", logger)
    validation_status = validation_status & validate(len(df['ICT Mill Names']),
                                                     len(df['Yellow_mill_name']),
                                                     "Yellow mill names missing values", logger)
    validation_status = validation_status & validate(len(df['Yellow_mill_name'].unique()),
                                                     len(df['Yellow_mill_name']),
                                                     "Yellow mill names duplication", logger)
    return validation_status


def check_map_mill_grade_file(df, logger):
    validation_status = validate(len(df['Grade']),
                                 len(df['Grade'].unique()),
                                 "Grade duplication", logger)
    return validation_status


def check_list_competitor_file(df, logger):
    # Filters non relevant competitors
    df = df[df['relevant competitors'] == 'x']
    df['key'] = df['ict_manufacturer_group'].astype(str) + \
                                     df['ict_country'].astype(str)

    validation_status = validate(len(df['key'].unique()),
                                 len(df['ict_manufacturer_group']),
                                 "manufacturer group duplication", logger)
    return validation_status


def validate(expr, expected_result, expr_desc, logger):
    """
    Validation utility method takes an expression and a result as inputs and returns True if validation passed
    False otherwise with a description of the failed step (expr_desc)
    """
    validation_status = (expr == expected_result)

    if not validation_status:
        logger.error("Failed to validate step : {}".format(expr_desc))

    return validation_status
