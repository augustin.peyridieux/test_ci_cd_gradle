import sys
sys.path.append("utils/")
sys.path.append("../dependencies/utils/")
sys.path.append("pricing_project_industry-master/code/dependencies/utils/")
sys.path.append("../dependencies/utils/")
import utils
import utils_validation as uv
import boto3
import pandas as pd

logger = utils.create_logger('Input files validation process')

logger.info("Starting: " + str(sys.argv[0]))

args = uv.get_parameters(logger)

validation_result = True

# Getting S3 client
logger.info("Getting S3 client")

s3_resource = boto3.resource('s3')
my_bucket = s3_resource.Bucket(name=args.bucket_name)

global_validation = True

# Checking grouping_tech_var_file
logger.info("Downloading AWS file: " + args.aws_folder_raw + args.grouping_tech_var_file)
my_bucket.download_file(args.aws_folder_raw + args.grouping_tech_var_file, args.grouping_tech_var_file)

dico = utils.read_grouping_tech_var_file(args.grouping_tech_var_file, logger)

global_validation = uv.check_grouping_tech_var_file(dico, logger)

# Checking map_enduser_file
logger.info("Downloading AWS file: " + args.aws_folder_raw + args.map_enduser_file)
my_bucket.download_file(args.aws_folder_raw + args.map_enduser_file, args.map_enduser_file)

xl = pd.ExcelFile(args.map_enduser_file, engine='openpyxl')
enduser_mapping_df = xl.parse(sheet_name=1)

global_validation = global_validation & uv.check_map_enduser_file(enduser_mapping_df, logger)

# Checking map_orderer_file
logger.info("Downloading AWS file: " + args.aws_folder_raw + args.map_orderer_file)
my_bucket.download_file(args.aws_folder_raw + args.map_orderer_file, args.map_orderer_file)

xl = pd.ExcelFile(args.map_orderer_file, engine='openpyxl')
orderer_mapping_df = xl.parse(sheet_name=1)

global_validation = global_validation & uv.check_map_orderer_file(orderer_mapping_df, logger)

# Check grade_competitor_file file
logger.info("Downloading AWS file: " + args.aws_folder_raw + args.grade_competitor_file)
my_bucket.download_file(args.aws_folder_raw + args.grade_competitor_file, args.grade_competitor_file)

grade_competitor_df = pd.read_excel(args.grade_competitor_file)

global_validation = global_validation & uv.check_grade_competitor_file(grade_competitor_df, logger)

# Check od_wt_ref_file
logger.info("Downloading AWS file: " + args.aws_folder_raw + args.od_wt_ref_file)
my_bucket.download_file(args.aws_folder_raw + args.od_wt_ref_file, args.od_wt_ref_file)

od_wt_ref_df = pd.read_csv(args.od_wt_ref_file, index_col="Unnamed: 0")

global_validation = global_validation & uv.check_od_wt_ref_file(od_wt_ref_df, logger)

# Check map_marketing_segment_file
logger.info("Downloading AWS file: " + args.aws_folder_raw + args.map_marketing_segment_file)
my_bucket.download_file(args.aws_folder_raw + args.map_marketing_segment_file, args.map_marketing_segment_file)

market_seg_df = pd.read_csv(args.map_marketing_segment_file, encoding='latin-1', sep=';')

global_validation = global_validation & uv.check_map_marketing_segment_file(market_seg_df, logger)

# Check map_mill_ict_yellow_file
logger.info("Downloading AWS file: " + args.aws_folder_raw + args.map_mill_ict_yellow_file)
my_bucket.download_file(args.aws_folder_raw + args.map_mill_ict_yellow_file, args.map_mill_ict_yellow_file)

mill_ict_df = pd.read_csv(args.map_mill_ict_yellow_file)

global_validation = global_validation & uv.check_map_mill_ict_yellow_file(mill_ict_df, logger)

# Check map_mill_grade_file
logger.info("Downloading AWS file: " + args.aws_folder_raw + args.map_mill_grade_file)
my_bucket.download_file(args.aws_folder_raw + args.map_mill_grade_file, args.map_mill_grade_file)

mapping_grade_df = pd.read_excel(args.map_mill_grade_file, index_col="Unnamed: 0")

global_validation = global_validation & uv.check_map_mill_grade_file(mapping_grade_df, logger)

# Check list_competitor_file
logger.info("Downloading AWS file: " + args.aws_folder_raw + args.list_competitor_file)
my_bucket.download_file(args.aws_folder_raw + args.list_competitor_file, args.list_competitor_file)

relevant_competitors_df = pd.read_excel(args.list_competitor_file, index_col="Unnamed: 0")

global_validation = global_validation & uv.check_list_competitor_file(relevant_competitors_df, logger)

if not global_validation:
    logger.error("Global validation failed, check the logs for more information")
    sys.exit(1)

logger.info("Global validation done successfully.")




