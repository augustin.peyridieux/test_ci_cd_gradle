import sys

sys.path.append("utils/")
sys.path.append("../dependencies/utils/")
sys.path.append("pricing_project_industry-master/code/dependencies/utils/")
import utils
import common_business_rules as cbr
import numpy as np
import pandas as pd
import argparse


def get_parameters(logger):
    """Get program's parameters

    Args:
        logger (Logger): Logger

    Returns:
        args: Return args if all args a correct and filled, raise Error if not
    """

    logger.info("Getting parameters")

    desc = "Pricing Project Industry: Merge ICT and Yellowfin files"

    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("bucket_name",
                        help="Name of the AWS S3 bucket name")
    parser.add_argument("aws_folder_raw",
                        help=("Name of the AWS S3 folder raw"))
    parser.add_argument("aws_folder_intermediate",
                        help=("Name of the AWS S3 folder intermediate"))
    parser.add_argument("ict_round_file",
                        help=("Name of the ICT round file"))
    parser.add_argument("ict_square_file",
                        help=("Name of the ICT square file"))
    parser.add_argument("map_mill_grade_file",
                        help=("Name of the mapping Mill Grade file"))
    parser.add_argument("od_wt_ref_file",
                        help=("Name of the OD WT referential file"))
    parser.add_argument("od_wt_ref_file_square",
                        help=("Name of the width height referential square file"))
    parser.add_argument("list_competitor_file",
                        help=("Name of the list of relevant file"))
    parser.add_argument("output_file",
                        help=("Name of the output file"))
                        

    args = parser.parse_args()

    args.aws_folder_raw = utils.add_backslash(args.aws_folder_raw)
    args.aws_folder_intermediate = utils.add_backslash(args.aws_folder_intermediate)

    logger.info("bucket_name: " + str(args.bucket_name))
    logger.info("aws_folder_raw: " + str(args.aws_folder_raw))
    logger.info("aws_folder_intermediate: " + str(args.aws_folder_intermediate))
    logger.info("ict_round_file: " + str(args.ict_round_file))
    logger.info("ict_square_file: " + str(args.ict_square_file))
    logger.info("map_mill_grade_file: " + str(args.map_mill_grade_file))
    logger.info("od_wt_ref_file: " + str(args.od_wt_ref_file))
    logger.info("od_wt_ref_file_square: " + str(args.od_wt_ref_file_square))
    logger.info("list_competitor_file: " + str(args.list_competitor_file))
    logger.info("output_file: " + str(args.output_file))
    return args


def download_files(my_bucket, args, logger):
    """Download files from AWS S3

    Args:
        my_bucket ([String]): AWS Bucket name
        args (args): arguments of the program
        logger (logger): logger

    Returns:
        int: 0 if all went fine, Error if file is missing
    """
    logger.info("Download AWS file: " + args.aws_folder_raw + args.map_mill_grade_file)
    my_bucket.download_file(args.aws_folder_raw + args.map_mill_grade_file, args.map_mill_grade_file)

    logger.info("Download AWS file: " + args.aws_folder_intermediate + args.ict_round_file)
    my_bucket.download_file(args.aws_folder_intermediate + args.ict_round_file, args.ict_round_file)

    logger.info("Download AWS file: " + args.aws_folder_intermediate + args.ict_square_file)
    my_bucket.download_file(args.aws_folder_intermediate + args.ict_square_file, args.ict_square_file)

    logger.info("Download AWS file: " + args.aws_folder_raw + args.list_competitor_file)
    my_bucket.download_file(args.aws_folder_raw + args.list_competitor_file, args.list_competitor_file)

    logger.info("Download AWS file: " + args.aws_folder_raw + args.od_wt_ref_file)
    my_bucket.download_file(args.aws_folder_raw + args.od_wt_ref_file, args.od_wt_ref_file)

    logger.info("Download AWS file: " + args.aws_folder_raw + args.od_wt_ref_file_square)
    my_bucket.download_file(args.aws_folder_raw + args.od_wt_ref_file_square, args.od_wt_ref_file_square)

    logger.info("Download AWS file: " + args.aws_folder_raw + args.list_competitor_file)
    my_bucket.download_file(args.aws_folder_raw + args.list_competitor_file, args.list_competitor_file)
    return 0


def get_ict_round(args, logger, list_country_eu_geo):
    """Read, filter and rename columns of ICT round file

    Args:
        args ([args]): [argument of the program]
        logger ([logger]): [logger]
        list_country_eu_geo ([List]): [List of considered EU countries]

    Returns:
        [type]: [description]
    """
    logger.info("Get Dataframe ICT round")
    df_ict_ext_round = utils.strip_df_col_values(pd.read_csv(args.ict_round_file)).drop(columns='highest_grade')

    df_ict_ext_round = cbr.filter_mills(df_ict_ext_round, 'Mill_Name')

    df_ict_ext_round = df_ict_ext_round[['Length', 'grade_classes_of_products', 'Company_Group', 'Mill_Name', 'Country'
        , 'Manufacturing_process_list', 'Qualification_list', 'Theoretical_max_length_in_m'
        , 'source_date', 'OD(mm)', 'AWT(mm)']] \
        .rename(columns={'Length': 'ict_length'
        , 'grade_classes_of_products': 'ict_grade_classes_of_products'
        , 'Company_Group': 'ict_manufacturer_group'
        , 'Mill_Name': 'ict_mill_manufacturer_name'
        , 'Country': 'ict_country'
        , 'Manufacturing_process_list': 'ict_manufacturing_process_list'
        , 'Qualification_list': 'ict_qualification_list'
        , 'Theoretical_max_length_in_m': 'ict_theoretical_max_length_in_m'
        , 'source_date': 'ict_source_date'
        , 'OD(mm)': 'ict_od_mm'
        , 'AWT(mm)': 'ict_awt_mm'})

    # df_ict_ext_round = df_ict_ext_round[df_ict_ext_round['ict_country'].isin(list_country_eu_geo)]
    df_ict_ext_round['ict_grade_classes_of_products'] = df_ict_ext_round['ict_grade_classes_of_products'].str.lower()
    df_ict_ext_round = cbr.filter_manufacturer_group(df_ict_ext_round)
    df_ict_ext_round['ict_length'] = df_ict_ext_round['ict_length'].apply \
        (lambda value: None if (str(value).lower() == 'x') | (value == '----') else value)
    logger.info("len df_ict_ext_round: " + str(len(df_ict_ext_round)))
    return df_ict_ext_round


def filter_competitor(df_ict_ext_round, args, logger):
    """filter ICT dataFrame on relevant competitor

    Args:
        df ([DataFrame]): [df to filter]
        args ([args]): [containing all the input of the program, in our case the list of relevant competitor file]
        logger ([logger]): [the logger]

    Returns:
        [DataFrame]: [df filtered]
    """
    logger.info("Filter relevant competitors")
    df_list_competitor = pd.read_excel(args.list_competitor_file)

    df_list_competitor = df_list_competitor[df_list_competitor['relevant competitors'] == 'x'][[
            "ict_manufacturer_group", "ict_country"]]

    # sanitize Company_Group column
    df_list_competitor = utils.sanitize_str_df(df_list_competitor, ['ict_manufacturer_group'], logger)

    logger.info("df_list_competitor length (start filter_competitor) {}".format(len(df_list_competitor)))
    logger.info("df_list_competitor (start filter_competitor) {}".format(df_list_competitor.to_string()))
    logger.info("df_list_competitor ict_manufacturer_group (start filter_competitor) {}".format(
        df_list_competitor['ict_manufacturer_group'].unique()))
    logger.info("df_ict_ext_round ict_manufacturer_group (start filter_competitor) {}".format(
        df_ict_ext_round['ict_manufacturer_group'].unique()))

    df_ict_ext_round_country = pd.merge(left=df_ict_ext_round,
                                        right=df_list_competitor[~df_list_competitor['ict_country'].isna()],
                                        how="inner", on=['ict_manufacturer_group', 'ict_country'])
    df_ict_ext_round_no_country = pd.merge(left=df_ict_ext_round,
                                        right=df_list_competitor[df_list_competitor['ict_country'].isna()][['ict_manufacturer_group']],
                                        how="inner", on=['ict_manufacturer_group'])
    df_ict_ext_round = pd.concat([df_ict_ext_round_country, df_ict_ext_round_no_country])
    df_ict_ext_round['ict_mill_manufacturer_name'] = df_ict_ext_round['ict_mill_manufacturer_name'].apply(lambda x: None if x == 'all' else x)

    logger.info("df_ict_ext_round ict_manufacturer_group (end filter_competitor) {}".format(df_ict_ext_round['ict_manufacturer_group'].unique()))

    logger.info("df_ict_ext_round length (end filter_competitor) {}".format(len(df_ict_ext_round)))
    logger.info("len df_ict_ext_round: " + str(len(df_ict_ext_round)))
    return df_ict_ext_round


def get_ict_square(args, logger, list_country_eu_geo):
    """Reading, renaming and filter ICT square files

    Args:
        args ([args]): [argument of the program]
        logger ([logger]): [logger]
        list_country_eu_geo ([List]): [List of EU countries]

    Returns:
        [DataFrame]: [ICT square Dataframe]
    """
    logger.info("Get Dataframe ICT square")
    df_ict_ext_square = utils.strip_df_col_values(pd.read_csv(args.ict_square_file))
    df_ict_ext_square = cbr.filter_mills(df_ict_ext_square, 'Mill_Name')
    df_ict_ext_square = df_ict_ext_square[
        ['Length', 'grade_classes_of_products', 'Company_Group', 'Mill_Name', 'Country'
            , 'Manufacturing_process_list', 'Qualification_list', 'Theoretical_max_length_in_m'
            , 'source_date', 'AWT(mm)', 'Height', 'Width']] \
        .rename(columns={'Length': 'ict_length'
        , 'grade_classes_of_products': 'ict_grade_classes_of_products'
        , 'Company_Group': 'ict_manufacturer_group'
        , 'Mill_Name': 'ict_mill_manufacturer_name'
        , 'Country': 'ict_country'
        , 'Manufacturing_process_list': 'ict_manufacturing_process_list'
        , 'Qualification_list': 'ict_qualification_list'
        , 'Theoretical_max_length_in_m': 'ict_theoretical_max_length_in_m'
        , 'source_date': 'ict_source_date'
        , 'AWT(mm)': 'ict_awt_mm'
        , 'Height': 'ict_height'
        , 'Width': 'ict_width'})

    # df_ict_ext_square = df_ict_ext_square[df_ict_ext_square['ict_country'].isin(list_country_eu_geo)]
    df_ict_ext_square['ict_grade_classes_of_products'] = df_ict_ext_square['ict_grade_classes_of_products'].str.lower()
    logger.info("len df_ict_ext_square: " + str(len(df_ict_ext_square)))
    return df_ict_ext_square


def add_missing_round_tubes(df_ict_ext_round, df_od_wt_ref, logger):
    """
    Add missing tubes:
    TODO: Optimize by looping only once threw ict_grade_classes_of_products, ict_mill_manufacturer_name,
    ict_manufacturer_group, ict_manufacturing_process_list
    Add missing square tubes when applicable.
    When a company (including Vallourec) is building tubes at 2 Height x Width and / or WT, we consider that he is also building
    tubes for all tubes in the middle.
    eg:
    if a company is building tubes for:
    Height = 150 and 180
    we consider that he is building tubes also for:
    Height = 160, 170
    And the same for WT.
    :param df_ict_ext_round:
    :param df_od_wt_ref:
    :return:
    """
    logger.info("Add missing round tubes")
    new_lines = pd.DataFrame(columns=df_ict_ext_round.columns)

    for ict_grade_class in df_ict_ext_round['ict_grade_classes_of_products'].unique():
        for ict_mill_name in df_ict_ext_round[df_ict_ext_round['ict_grade_classes_of_products'] == ict_grade_class] \
                ['ict_mill_manufacturer_name'].unique():
            for ict_group in df_ict_ext_round[(df_ict_ext_round['ict_grade_classes_of_products'] == ict_grade_class) \
                                              & (df_ict_ext_round['ict_mill_manufacturer_name'] == ict_mill_name)] \
                    ['ict_manufacturer_group'].unique():

                # if ict_group != 'Vallourec':
                for ict_manufact_proc in \
                        df_ict_ext_round[(df_ict_ext_round['ict_grade_classes_of_products'] == ict_grade_class) \
                                         & (df_ict_ext_round['ict_mill_manufacturer_name'] == ict_mill_name) \
                                         & (df_ict_ext_round['ict_manufacturer_group'] == ict_group)] \
                                ['ict_manufacturing_process_list'].unique():
                    for ict_wt in \
                            df_ict_ext_round[(df_ict_ext_round['ict_grade_classes_of_products'] == ict_grade_class) \
                                             & (df_ict_ext_round['ict_mill_manufacturer_name'] == ict_mill_name) \
                                             & (df_ict_ext_round['ict_manufacturer_group'] == ict_group) \
                                             & (df_ict_ext_round[
                                                    'ict_manufacturing_process_list'] == ict_manufact_proc)] \
                                    ['map_ref_val_wall_thickness'].unique():

                        df_tmp = df_ict_ext_round[(df_ict_ext_round['ict_grade_classes_of_products'] == ict_grade_class) \
                                                  & (df_ict_ext_round['ict_mill_manufacturer_name'] == ict_mill_name) \
                                                  & (df_ict_ext_round['ict_manufacturer_group'] == ict_group) \
                                                  & (df_ict_ext_round[
                                                         'ict_manufacturing_process_list'] == ict_manufact_proc) \
                                                  & (df_ict_ext_round['map_ref_val_wall_thickness'] == ict_wt)]

                        min_od = min(df_tmp['map_ref_val_diameter_height'])
                        max_od = max(df_tmp['map_ref_val_diameter_height'])

                        list_missing = list(set(df_od_wt_ref[(df_od_wt_ref['ref_val_od_mm'] >= min_od) \
                                                             & (df_od_wt_ref['ref_val_od_mm'] <= max_od)][
                                                    'ref_val_od_mm'].unique()) \
                                            - set(df_tmp[(df_tmp['map_ref_val_diameter_height'] >= min_od) \
                                                         & (df_tmp['map_ref_val_diameter_height'] <= max_od)][
                                                      'map_ref_val_diameter_height'].unique()))
                        for el in list_missing:
                            new_line = df_tmp.iloc[0]
                            new_line = pd.DataFrame(new_line).T
                            new_line['map_ref_val_diameter_height'] = el
                            new_line['ict_awt_mm'] = np.nan
                            new_line['ict_od_mm'] = np.nan
                            new_lines = pd.concat([new_line, new_lines], sort=False)

    new_lines = new_lines.drop_duplicates()
    df_ict_ext_round = pd.concat([new_lines, df_ict_ext_round], sort=False)
    new_lines = pd.DataFrame(columns=df_ict_ext_round.columns)

    for ict_grade_class in df_ict_ext_round['ict_grade_classes_of_products'].unique():
        for ict_mill_name in df_ict_ext_round[df_ict_ext_round['ict_grade_classes_of_products'] == ict_grade_class] \
                ['ict_mill_manufacturer_name'].unique():
            for ict_group in df_ict_ext_round[(df_ict_ext_round['ict_grade_classes_of_products'] == ict_grade_class) \
                                              & (df_ict_ext_round['ict_mill_manufacturer_name'] == ict_mill_name)] \
                    ['ict_manufacturer_group'].unique():

                for ict_manufact_proc in \
                        df_ict_ext_round[(df_ict_ext_round['ict_grade_classes_of_products'] == ict_grade_class) \
                                         & (df_ict_ext_round['ict_mill_manufacturer_name'] == ict_mill_name) \
                                         & (df_ict_ext_round['ict_manufacturer_group'] == ict_group)] \
                                ['ict_manufacturing_process_list'].unique():
                    for ict_od in \
                            df_ict_ext_round[(df_ict_ext_round['ict_grade_classes_of_products'] == ict_grade_class) \
                                             & (df_ict_ext_round['ict_mill_manufacturer_name'] == ict_mill_name) \
                                             & (df_ict_ext_round['ict_manufacturer_group'] == ict_group) \
                                             & (df_ict_ext_round[
                                                    'ict_manufacturing_process_list'] == ict_manufact_proc)] \
                                    ['map_ref_val_diameter_height'].unique():

                        df_tmp = df_ict_ext_round[(df_ict_ext_round['ict_grade_classes_of_products'] == ict_grade_class) \
                                                  & (df_ict_ext_round['ict_mill_manufacturer_name'] == ict_mill_name) \
                                                  & (df_ict_ext_round['ict_manufacturer_group'] == ict_group) \
                                                  & (df_ict_ext_round[
                                                         'ict_manufacturing_process_list'] == ict_manufact_proc) \
                                                  & (df_ict_ext_round['map_ref_val_diameter_height'] == ict_od)]

                        min_wt = min(df_tmp['map_ref_val_wall_thickness'])
                        max_wt = max(df_tmp['map_ref_val_wall_thickness'])

                        list_missing = list(set(df_od_wt_ref[(df_od_wt_ref['ref_val_wt_mm'] >= min_wt) \
                                                             & (df_od_wt_ref['ref_val_wt_mm'] <= max_wt)][
                                                    'ref_val_wt_mm'].unique()) \
                                            - set(df_tmp[(df_tmp['map_ref_val_wall_thickness'] >= min_wt) \
                                                         & (df_tmp['map_ref_val_wall_thickness'] <= max_wt)][
                                                      'map_ref_val_wall_thickness'].unique()))
                        for el in list_missing:
                            new_line = df_tmp.iloc[0]
                            new_line = pd.DataFrame(new_line).T
                            new_line['map_ref_val_wall_thickness'] = el
                            new_line['ict_awt_mm'] = np.nan
                            new_line['ict_od_mm'] = np.nan
                            new_lines = pd.concat([new_line, new_lines])
    new_lines = new_lines.drop_duplicates()
    df_ict_ext_round = pd.concat([new_lines, df_ict_ext_round], sort=False)
    logger.info("len df_ict_ext_round: " + str(len(df_ict_ext_round)))
    return df_ict_ext_round


def add_missing_square_tubes(df_ict_ext_square, df_od_wt_ref_square, logger):
    """
    TODO: Optimize by looping only once threw ict_grade_classes_of_products, ict_mill_manufacturer_name,
    ict_manufacturer_group, ict_manufacturing_process_list
    Add missing square tubes when applicable.
    if a company is building pipes for a certain range of size. If one size in missing in the middle, we consider
    that this company is also building the pipes for missing sizes in the middle


    Args:
        df_ict_ext_square ([DataFrame]): [input ICT Square pipes]
        df_od_wt_ref_square ([type]): [Square pipes vallourec reference]
        logger ([logger]): [logger]

    Returns:
        [DataFrame]: [Datframe with missing pipes added]
    """
    logger.info("Add missing tubes")
    new_lines = pd.DataFrame(columns=df_ict_ext_square.columns.tolist())

    # loop threw all unique values to identify missing tubes
    for ict_grade_class in df_ict_ext_square['ict_grade_classes_of_products'].unique():
        for ict_mill_name in df_ict_ext_square[df_ict_ext_square['ict_grade_classes_of_products'] == ict_grade_class] \
                ['ict_mill_manufacturer_name'].unique():
            for ict_group in df_ict_ext_square[(df_ict_ext_square['ict_grade_classes_of_products'] == ict_grade_class) \
                                            & (df_ict_ext_square['ict_mill_manufacturer_name'] == ict_mill_name)] \
                    ['ict_manufacturer_group'].unique():

                for ict_manufact_proc in \
                        df_ict_ext_square[(df_ict_ext_square['ict_grade_classes_of_products'] == ict_grade_class) \
                                        & (df_ict_ext_square['ict_mill_manufacturer_name'] == ict_mill_name) \
                                        & (df_ict_ext_square['ict_manufacturer_group'] == ict_group)] \
                                ['ict_manufacturing_process_list'].unique():
                    for ict_height in \
                            df_ict_ext_square[(df_ict_ext_square['ict_grade_classes_of_products'] == ict_grade_class) \
                                            & (df_ict_ext_square['ict_mill_manufacturer_name'] == ict_mill_name) \
                                            & (df_ict_ext_square['ict_manufacturer_group'] == ict_group) \
                                            & (df_ict_ext_square[
                                                    'ict_manufacturing_process_list'] == ict_manufact_proc)] \
                                    ['map_ref_val_diameter_height'].unique():
                        for ict_wt in \
                            df_ict_ext_square[(df_ict_ext_square['ict_grade_classes_of_products'] == ict_grade_class) \
                                            & (df_ict_ext_square['ict_mill_manufacturer_name'] == ict_mill_name) \
                                            & (df_ict_ext_square['ict_manufacturer_group'] == ict_group) \
                                            & (df_ict_ext_square['ict_manufacturing_process_list'] == ict_manufact_proc) \
                                            & (df_ict_ext_square['map_ref_val_diameter_height'] == ict_height)] \
                                    ['map_ref_val_wall_thickness'].unique():

                            df_tmp = df_ict_ext_square[(df_ict_ext_square['ict_grade_classes_of_products'] == ict_grade_class) \
                                                    & (df_ict_ext_square['ict_mill_manufacturer_name'] == ict_mill_name) \
                                                    & (df_ict_ext_square['ict_manufacturer_group'] == ict_group) \
                                                    & (df_ict_ext_square['ict_manufacturing_process_list'] == ict_manufact_proc) \
                                                    & (df_ict_ext_square['map_ref_val_diameter_height'] == ict_height) \
                                                    & (df_ict_ext_square['map_ref_val_wall_thickness'] == ict_wt)]

                            #identify the extremity of the size
                            min_width = min(df_tmp['map_ref_val_width'])
                            max_width = max(df_tmp['map_ref_val_width'])

                            # get a list of missing sizes
                            list_missing = list(set(df_od_wt_ref_square[(df_od_wt_ref_square['ref_val_width_mm'] >= min_width) \
                                                                & (df_od_wt_ref_square['ref_val_width_mm'] <= max_width) \
                                                                & (df_od_wt_ref_square['ref_val_height_mm'] == ict_height) \
                                                                & (df_od_wt_ref_square['ref_val_wt_mm'] == ict_wt)][
                                                                'ref_val_width_mm'].unique()) \
                                                - set(df_tmp[(df_tmp['map_ref_val_width'] >= min_width) \
                                                            & (df_tmp['map_ref_val_width'] <= max_width)][
                                                            'map_ref_val_width'].unique()))

                            # create for each missing size a new line with the same information
                            for el in list_missing:
                                new_line = df_tmp.iloc[0]
                                new_line = pd.DataFrame(new_line).T
                                new_line['map_ref_val_width'] = el
                                new_line['ict_awt_mm'] = np.nan
                                new_line['ict_height'] = np.nan
                                new_line['ict_width'] = np.nan
                                new_lines = pd.concat([new_line, new_lines], sort=False)


    for ict_grade_class in df_ict_ext_square['ict_grade_classes_of_products'].unique():
        for ict_mill_name in df_ict_ext_square[df_ict_ext_square['ict_grade_classes_of_products'] == ict_grade_class] \
                ['ict_mill_manufacturer_name'].unique():
            for ict_group in df_ict_ext_square[(df_ict_ext_square['ict_grade_classes_of_products'] == ict_grade_class) \
                                            & (df_ict_ext_square['ict_mill_manufacturer_name'] == ict_mill_name)] \
                    ['ict_manufacturer_group'].unique():

                for ict_manufact_proc in \
                        df_ict_ext_square[(df_ict_ext_square['ict_grade_classes_of_products'] == ict_grade_class) \
                                        & (df_ict_ext_square['ict_mill_manufacturer_name'] == ict_mill_name) \
                                        & (df_ict_ext_square['ict_manufacturer_group'] == ict_group)] \
                                ['ict_manufacturing_process_list'].unique():
                    for ict_width in \
                            df_ict_ext_square[(df_ict_ext_square['ict_grade_classes_of_products'] == ict_grade_class) \
                                            & (df_ict_ext_square['ict_mill_manufacturer_name'] == ict_mill_name) \
                                            & (df_ict_ext_square['ict_manufacturer_group'] == ict_group) \
                                            & (df_ict_ext_square[
                                                    'ict_manufacturing_process_list'] == ict_manufact_proc)] \
                                    ['map_ref_val_width'].unique():
                        for ict_wt in \
                            df_ict_ext_square[(df_ict_ext_square['ict_grade_classes_of_products'] == ict_grade_class) \
                                            & (df_ict_ext_square['ict_mill_manufacturer_name'] == ict_mill_name) \
                                            & (df_ict_ext_square['ict_manufacturer_group'] == ict_group) \
                                            & (df_ict_ext_square['ict_manufacturing_process_list'] == ict_manufact_proc) \
                                            & (df_ict_ext_square['map_ref_val_width'] == ict_width)] \
                                    ['map_ref_val_wall_thickness'].unique():

                            df_tmp = df_ict_ext_square[(df_ict_ext_square['ict_grade_classes_of_products'] == ict_grade_class) \
                                                    & (df_ict_ext_square['ict_mill_manufacturer_name'] == ict_mill_name) \
                                                    & (df_ict_ext_square['ict_manufacturer_group'] == ict_group) \
                                                    & (df_ict_ext_square['ict_manufacturing_process_list'] == ict_manufact_proc) \
                                                    & (df_ict_ext_square['map_ref_val_width'] == ict_width) \
                                                    & (df_ict_ext_square['map_ref_val_wall_thickness'] == ict_wt)]

                            min_height = min(df_tmp['map_ref_val_diameter_height'])
                            max_height = max(df_tmp['map_ref_val_diameter_height'])

                            list_missing = list(set(df_od_wt_ref_square[(df_od_wt_ref_square['ref_val_height_mm'] >= min_height) \
                                                                & (df_od_wt_ref_square['ref_val_height_mm'] <= max_height) \
                                                                & (df_od_wt_ref_square['ref_val_width_mm'] == ict_width) \
                                                                & (df_od_wt_ref_square['ref_val_wt_mm'] == ict_wt)][
                                                                'ref_val_height_mm'].unique()) \
                                                - set(df_tmp[(df_tmp['map_ref_val_diameter_height'] >= min_height) \
                                                            & (df_tmp['map_ref_val_diameter_height'] <= max_height)][
                                                            'map_ref_val_diameter_height'].unique()))
                            for el in list_missing:
                                new_line = df_tmp.iloc[0]
                                new_line = pd.DataFrame(new_line).T
                                new_line['map_ref_val_diameter_height'] = el
                                new_line['ict_awt_mm'] = np.nan
                                new_line['ict_height'] = np.nan
                                new_line['ict_width'] = np.nan
                                new_lines = pd.concat([new_line, new_lines], sort=False)


    for ict_grade_class in df_ict_ext_square['ict_grade_classes_of_products'].unique():
        for ict_mill_name in df_ict_ext_square[df_ict_ext_square['ict_grade_classes_of_products'] == ict_grade_class] \
                ['ict_mill_manufacturer_name'].unique():
            for ict_group in df_ict_ext_square[(df_ict_ext_square['ict_grade_classes_of_products'] == ict_grade_class) \
                                            & (df_ict_ext_square['ict_mill_manufacturer_name'] == ict_mill_name)] \
                    ['ict_manufacturer_group'].unique():

                for ict_manufact_proc in \
                        df_ict_ext_square[(df_ict_ext_square['ict_grade_classes_of_products'] == ict_grade_class) \
                                        & (df_ict_ext_square['ict_mill_manufacturer_name'] == ict_mill_name) \
                                        & (df_ict_ext_square['ict_manufacturer_group'] == ict_group)] \
                                ['ict_manufacturing_process_list'].unique():
                    for ict_width in \
                            df_ict_ext_square[(df_ict_ext_square['ict_grade_classes_of_products'] == ict_grade_class) \
                                            & (df_ict_ext_square['ict_mill_manufacturer_name'] == ict_mill_name) \
                                            & (df_ict_ext_square['ict_manufacturer_group'] == ict_group) \
                                            & (df_ict_ext_square[
                                                    'ict_manufacturing_process_list'] == ict_manufact_proc)] \
                                    ['map_ref_val_width'].unique():
                        for ict_height in \
                            df_ict_ext_square[(df_ict_ext_square['ict_grade_classes_of_products'] == ict_grade_class) \
                                            & (df_ict_ext_square['ict_mill_manufacturer_name'] == ict_mill_name) \
                                            & (df_ict_ext_square['ict_manufacturer_group'] == ict_group) \
                                            & (df_ict_ext_square['ict_manufacturing_process_list'] == ict_manufact_proc) \
                                            & (df_ict_ext_square['map_ref_val_width'] == ict_width)] \
                                    ['map_ref_val_diameter_height'].unique():

                            df_tmp = df_ict_ext_square[(df_ict_ext_square['ict_grade_classes_of_products'] == ict_grade_class) \
                                                    & (df_ict_ext_square['ict_mill_manufacturer_name'] == ict_mill_name) \
                                                    & (df_ict_ext_square['ict_manufacturer_group'] == ict_group) \
                                                    & (df_ict_ext_square['ict_manufacturing_process_list'] == ict_manufact_proc) \
                                                    & (df_ict_ext_square['map_ref_val_width'] == ict_width) \
                                                    & (df_ict_ext_square['map_ref_val_diameter_height'] == ict_height)]
                            # map_ref_val_width  map_ref_val_wall_thickness map_ref_val_diameter_height
                            min_wt = min(df_tmp['map_ref_val_wall_thickness'])
                            max_wt = max(df_tmp['map_ref_val_wall_thickness'])

                            list_missing = list(set(df_od_wt_ref_square[(df_od_wt_ref_square['ref_val_wt_mm'] >= min_wt) \
                                                                & (df_od_wt_ref_square['ref_val_wt_mm'] <= max_wt) \
                                                                & (df_od_wt_ref_square['ref_val_width_mm'] == ict_width) \
                                                                & (df_od_wt_ref_square['ref_val_height_mm'] == ict_height)][
                                                                'ref_val_wt_mm'].unique()) \
                                                - set(df_tmp[(df_tmp['map_ref_val_wall_thickness'] >= min_wt) \
                                                            & (df_tmp['map_ref_val_wall_thickness'] <= max_wt)][
                                                            'map_ref_val_wall_thickness'].unique()))

                            for el in list_missing:
                                new_line = df_tmp.iloc[0]
                                new_line = pd.DataFrame(new_line).T
                                new_line['map_ref_val_wall_thickness'] = el
                                new_line['ict_awt_mm'] = np.nan
                                new_line['ict_height'] = np.nan
                                new_line['ict_width'] = np.nan
                                new_lines = pd.concat([new_line, new_lines], sort=False)
    new_lines = new_lines.drop_duplicates()
    df_ict_ext_square = pd.concat([new_lines, df_ict_ext_square], sort=False)
    logger.info("len df_ict_ext_square: " + str(len(df_ict_ext_square)))
    return df_ict_ext_square


def get_cpy_name_when_mill_null(row):
    """Get manufacturer name if mill name is null

    Args:
        row ([row]): [row to analyze]

    Returns:
        [String]: [Mill name to consider]
    """
    if (row['ict_mill_manufacturer_name'] == row['ict_mill_manufacturer_name']) \
            & (row['ict_mill_manufacturer_name'] != None) \
            & (row['ict_mill_manufacturer_name'] != np.nan) \
            & (row['ict_mill_manufacturer_name'] != 'nan'):
        return row['ict_mill_manufacturer_name']
    else:
        return row["ict_manufacturer_group"]


def order_ict_columns(df, logger):
    """Order columns for reading purposes

    Args:
        df (DataFrame): Input ICT dataframe
        logger (logger of the app): logger

    Returns:
        DataFrame: Dataframes with ordered columns
    """
    logger.info("Order ICT columns")
    return df[['ict_country', 'ict_manufacturer_group', 'ict_mill_manufacturer_name', 'pipe_type'
        , 'ict_grade_classes_of_products'
        , 'ict_manufacturing_process_list', 'ict_qualification_list', 'ict_source_date', 'ict_theoretical_max_length_in_m'
       , 'ict_awt_mm', 'ict_od_mm', 'ict_height', 'ict_width', 'ict_length'
       , 'map_ref_val_wall_thickness', 'map_ref_val_diameter_height', 'map_ref_val_width']]