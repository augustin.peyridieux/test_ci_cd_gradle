import sys
sys.path.append("utils/")
sys.path.append("../dependencies/utils/")
sys.path.append("pricing_project_industry-master/code/dependencies/utils/")
import utils
import common_business_rules as cbr
import utils_ict_harmo
import boto3
import sys
import pandas as pd

logger = utils.create_logger('PPI: Join Yellowfin ICT files')

logger.info("Starting: " + str(sys.argv[0]))

args = utils_ict_harmo.get_parameters(logger)

s3_resource = boto3.resource('s3')
my_bucket = s3_resource.Bucket(name=args.bucket_name)

version = utils.get_verision_data(my_bucket, args, logger, False)

utils_ict_harmo.download_files(my_bucket, args, logger)

## Treat Square file
df_ict_ext_square = utils_ict_harmo.get_ict_square(args, logger, cbr.list_country_eu_geo)

# Read ref square file
df_od_wt_ref_square = utils.strip_df_col_values(pd.read_csv(args.od_wt_ref_file_square)).drop(columns=['Unnamed: 0'])
df_od_wt_ref_square = df_od_wt_ref_square.add_prefix('ref_val_')

df_ict_ext_square = utils.get_closest_height_width_tube_from_vall_ref(df_ict_ext_square, df_od_wt_ref_square, 'ict_height', 'ict_width', logger)
df_ict_ext_square =  utils.get_closest_square_wt_tube_from_vall_ref(df_ict_ext_square, df_od_wt_ref_square, 'ict_awt_mm', logger)

df_ict_ext_square['ict_mill_manufacturer_name'] = df_ict_ext_square.apply(lambda row: utils_ict_harmo.get_cpy_name_when_mill_null(row),
                                                                        axis=1)
# Add missing tubes
df_ict_ext_square = utils_ict_harmo.add_missing_square_tubes(df_ict_ext_square, df_od_wt_ref_square, logger)                          

## Treat Round file
df_ict_ext_round = utils_ict_harmo.get_ict_round(args, logger, cbr.list_country_eu_geo)
df_ict_ext_round = utils_ict_harmo.filter_competitor(df_ict_ext_round, args, logger)

# Read Ref round file
df_od_wt_ref = utils.strip_df_col_values(pd.read_csv(args.od_wt_ref_file)).drop(columns=['Unnamed: 0'])
df_od_wt_ref = df_od_wt_ref.add_prefix('ref_val_')

# Get closest tubes
df_ict_ext_round = utils.get_closest_tubes_from_val_ref(df_ict_ext_round, df_od_wt_ref)

df_ict_ext_round['ict_mill_manufacturer_name'] = df_ict_ext_round.apply(lambda row: utils_ict_harmo.get_cpy_name_when_mill_null(row),
                                                                        axis=1)
# Add missing tubes
df_ict_ext_round = utils_ict_harmo.add_missing_round_tubes(df_ict_ext_round, df_od_wt_ref, logger)

# Merging ICT round and square
logger.info("Merge ICT round and square")
df_ict_ext_round["pipe_type"] = "round"
df_ict_ext_square["pipe_type"] = "square"
df_ict = pd.concat([df_ict_ext_round, df_ict_ext_square], sort=False)
logger.info("len df_ict: " + str(len(df_ict)))

# Perform join between round and square here
df_mapping_mill_grade_compet_val = utils.strip_df_col_values(pd.read_excel(args.map_mill_grade_file))
df_mapping_mill_grade_compet_val = df_mapping_mill_grade_compet_val[
    df_mapping_mill_grade_compet_val['val_grades'] == df_mapping_mill_grade_compet_val['val_grades']]
df_mapping_mill_grade_compet_val = pd.melt(df_mapping_mill_grade_compet_val, id_vars=["Competitors_grades"],
                                           var_name="Name", value_name="val_grade_class").sort_values(
                                            by='Competitors_grades').drop(columns=['Name']).dropna()
df_mapping_mill_grade_compet_val['val_grade_class'] = df_mapping_mill_grade_compet_val['val_grade_class'].str.lower()

logger.info("Merging df_ict and df_mapping_mill_grade_compet_val")
df_ict = pd.merge(left=df_ict
                    , right=df_mapping_mill_grade_compet_val
                    , how='left'
                    , left_on='ict_grade_classes_of_products'
                    , right_on='Competitors_grades') \
    .drop(columns=['ict_grade_classes_of_products', 'Competitors_grades']) \
    .rename(columns={'val_grade_class': 'ict_grade_classes_of_products'})[df_ict.columns]
logger.info("len df_ict: " + str(len(df_ict)))

df_ict = utils_ict_harmo.order_ict_columns(df_ict, logger)

logger.info("taille DataFrame: " + str(len(df_ict)))
#print(df_ict)
#print(version)
logger.info("Pushing final file on S3: " + args.output_file)
utils.store_df_s3_data_stats(df_ict, s3_resource, version, args, logger)