import logging
import pandas as pd
import numpy as np
import re

#filtered out mills
list_mill_to_filter_out = ['Reisholz', 'Zeithain']


#EU countries
list_country_eu_geo = ['Austria', 'Germany', 'Italy', 'France', 'Sweden',
       'Finland', 'Switzerland', 'Great Britain', 'Netherlands', 'Spain',
       'Norway', 'Czech Republic', 'Hungary', 'Belgium', 'Slovakia', 'Denmark', 'Poland',
         'Romania', 'Belarus', 'Greece', 'Ireland', 'Bulgaria', 'Estonia',
       'Croatia', 'Bosnia-Herzegovina', 'Serbia', 'Lithuania', 'Latvia', 'Ukraine', 'Portugal']


def fix_country_names(df, colname):
    """
    Fix incorrect country names coming from the ICT.
    """
    df[colname] = df[colname].replace({'CZ': 'Czech Republic', 'UK': 'Great Britain'})
    return df


def filter_mills(df, colname):
    """
    Returns a dataframe with the rows for which colname 
    value is not in list_mill_to_filter_out
    """
    return df[~df[colname].isin(list_mill_to_filter_out)]


def filter_eu_countries(df, colname='yfb_enduser_country', dropna=True):
    """
    Returns rows for which the country is defined in list_country_eu_geo.
    """
    if (dropna):
        return df[df[colname].isin(list_country_eu_geo)]
    else:
        return df[(df[colname].isin(list_country_eu_geo)) | df[colname].isna()]


def filter_manufacturer_group(df, colname='ict_manufacturer_group'):
    """
    Returns a dataframe with all the rows excepted those for which
    manufacturer group is equal to ESW
    """
    return df[df[colname] != 'ESW']


def filter_year_of_creation(df):
    """
    Returns all the rows for which year of creation is more than 2017.
    """
    return df[df['yfb_year_creation'] >= 2017]


def filter_non_numerical_order_number(df, colname, logger):
    """
    Returns only the orders defined by a numerical order number.
    """
    return df[df[colname].apply(lambda x: True if re.match(pattern=r"\d+", string=str(x)) else False)]


def filter_high_mtp_var_costs(df, logger):
    """
    Filters out high MTP var costs
    """
    return df[df['yfc_mill_var_cost'] < 9000.0]


def filter_low_high_brut_prices(df, logger):
    """
    Filters out too low and too high brut prices
    """
    return df[(df['yfc_gross_price'] < 10000.0) & (df['yfc_gross_price'] > 400.0)]


def calc_add_column(df, calc_fun, logger, colname = None):
    """
    Higher order function used to calculate a column value given a row
    """
    if colname is not None:
        df[colname] = np.nan

    df = df.apply(lambda row: calc_fun(row), axis=1)
    return df


def fix_zero_net_price(row):
    if row['yfc_net_price'] == 0.0:
        if row['yfc_gross_price'] == row['yfc_gross_price']:
            if row['yfc_freight_provision'] == row['yfc_freight_provision']:
                row['yfc_net_price'] = row['yfc_gross_price'] - row['yfc_freight_provision']
            else:
                row['yfc_net_price'] = row['yfc_gross_price']

    return row


def add_column_calc_mvc(row):
    """
    MVC = net price - var costs
    """
    if (row['yfc_net_price'] == row['yfc_net_price']) & (row['yfc_mill_var_cost'] == row['yfc_mill_var_cost']):
        row['calc_mvc'] = row['yfc_net_price'] - row['yfc_mill_var_cost']

    return row


def add_column_calc_mvc_tons(row):
    """
    Tot MVC = MVC * tons booked
    """
    if (row['calc_mvc'] == row['calc_mvc']) & (row['yfc_tons_booked_per_item'] == row['yfc_tons_booked_per_item']):
        row['calc_total_mvc'] = row['calc_mvc'] * row['yfc_tons_booked_per_item']
    return row


def add_column_calc_total_revenue(row):
    """
    Tot Revenue = Net price * tons booked
    """
    if (row['yfc_net_price'] == row['yfc_net_price']) &\
            (row['yfc_tons_booked_per_item'] == row['yfc_tons_booked_per_item']):
        row['calc_total_revenue'] = row['yfc_net_price'] * row['yfc_tons_booked_per_item']
    return row


def filter_non_relevant_order_number(df):
    """
    Returns orders for which the order number does not start with 8 and are 8 digits long.
    """
    return df[~((df['yfb_order_number'].astype(str).str.startswith('8'))
                & (df['yfb_order_number'].astype(str).str.len() == 8))]


def create_column_order_item(df, orderItemColname='yfb_order_item', orderNumberColname='yfb_order_number', itemNumberColname='yfb_item_number'):
    """
    Returns a dataframe with the order_item column.
    """
    df[orderItemColname] = df[orderNumberColname].astype(str) + '-' + df[itemNumberColname].astype(str).str.zfill(3)
    return df


def filter_high_diameter_pipes(df):
    """
    Returns only rows for which the diameter is lower than a threshold value.
    """
    return df[df['yfb_diameter_height'] < 661]


def filter_zero_diameter_or_wt(df):
    """
    Returns the rows for which at least one of the diameter or wall thickness is not zero.
    """
    return df[(df['yfb_diameter_height'] != 0) | (df['yfb_wall_thickness'] != 0)]


def calculate_inside_diameter(df):
    df['calc_inside_diameter'] = df['yfb_diameter_height'] - (2 * df['yfb_wall_thickness'])
    return df


def filter_low_tons_booked_per_item(df):
    """
    Filters out rows for which tons per item is lower than 1T.
    """
    return df[df['yfc_tons_booked_per_item'] >= 1]


def rename_grade_value(df, oldVal, newVal):
    """
    Renames a grade value by another one and return the dataframe
    """
    df['ict_grade_classes_of_products'] = df['ict_grade_classes_of_products']\
        .apply(lambda x: x if x != oldVal else newVal)

    return df

def calc_pipe_type(row):
    """Return the pipe type

    Args:
        row (row): Yellowfin DataFrame row

    Returns:
        String: pipe type
    """
    if row['yfb_dim_type_name_tmp'] is None:
        return 'round'
    if row['yfb_dim_type_name_tmp'] in ['OD x WT', 'Nominal OD x Nominal ID'
                                        , 'OD x MWT', 'ID x AWT',
                                        'Nominal OD x Nominal WT', 'ID x MWT',
                                        'Shaped pipes', 'OD x ID', 'Shaped or machined pipes']:
        return 'round'
    elif row['yfb_dim_type_name_tmp'] in ['Square : Side x Side x WT'
                                        , "Rectangular : Greater Length x  Smaller Length x WT"]:
        return 'square'
    elif row['yfb_dim_type_name_tmp'] in ['Hexagonal sections']:
        return 'hexagonal'
    else:
        return 'other'
