import logging
import unicodedata
import pandas as pd
import numpy as np


def sanitize_str(str):
    """
    Removes unwanted \xA0 characters
    """
    return unicodedata.normalize("NFKD",str).strip()


def sanitize_str_df(df, cols, logger):
    """
    Apply sanitize_str to all the columns in cols.
    """
    for col in cols:
        logger.info("Sanitizing column {} ...".format(col))
        df[col] = df[col].apply(lambda x: sanitize_str(str(x)))

    return df


def replace_missing_values(df):
    return df.replace({'NAN': np.nan, 'nan': np.nan, 'NO VALUE': np.nan, 'no value': np.nan, 'NaN': np.nan, np.nan: None})

def to_lowercase(df, colname):
    """
    Transform a dataframe column's values to lowercase
    """
    df[colname] = df[colname].str.lower()
    return df

def create_logger(name):
    logger = logging.getLogger(name)
    if not logger.handlers:
        logger.setLevel(logging.DEBUG)
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)

        # create formatter
        formatter = logging.Formatter(
            '%(asctime)s - %(levelname)s - %(filename)s %(lineno)d - %(message)s',
            datefmt='%d/%m/%y %H:%M:%S')

        # add formatter to ch
        ch.setFormatter(formatter)

        # add ch to logger
        logger.addHandler(ch)
    return logger


def add_backslash(path):
    """

    :param path:
    :return:
    """
    if not path.endswith('/'):
        return path + '/'
    else:
        return path


def convert_column_to_float(value):
    if (value != value) | (value == 'None') | (value is None):
        return value
    if str(value).count('.') == 2:
        val_splitted = str(value).split('.')
        value = val_splitted[0] + val_splitted[1] + '.' + val_splitted[2]
    elif str(value).count('.') + str(value).count(',') == 2:
        val_splitted = str(value).split('.')
        value = val_splitted[0] + val_splitted[1]

    return float(str(value).replace(' ', '').replace(',', '.'))


def strip_df_col_values(df):
    # Strip all columns to avoid joins issues
    df = df.applymap(lambda x: x.strip() if isinstance(x, str) else x)
    
    df.columns = df.columns.str.strip(to_strip = ' ')
    return df


def download_aws_directory(directory, boto_client, logger=None):
    """
    Download an AWS directory
    :param directory: Directory Name
    :param boto_client: AWS boto3 client
    :param logger: (optional) logger of the app
    :return: None if all files has been downloaded
    :return: Raise NameError if folder is empty
    """
    is_empty = True
    for aws_file in get_list_aws_file_in_directory(directory, boto_client):
        is_empty = False
        if logger:
            logger.info("Download file: " + aws_file)
        boto_client.download_file(aws_file, aws_file.split('/')[-1])
    if is_empty:
        raise NameError('folder "' + directory + '" is empty')


def get_list_aws_file_in_directory(directory, boto_client):
    """

    :param directory:
    :param boto_client:
    :param logger:
    :return:
    """
    list_aws_files = []
    for aws_object in boto_client.objects.filter(Prefix=directory):
        if not str(aws_object.key).endswith('/'):
            list_aws_files.append(aws_object.key)
    return list_aws_files


def move_aws_file(name_file_source, name_file_dest, my_bucket, logger=None):
    """

    :param name_file_source:
    :param name_file_dest:
    :param my_bucket:
    :param logger:
    :return:
    """
    logger.info("moving file '" + name_file_source + "' to : " + name_file_dest)
    if not name_file_source in get_list_aws_file_in_directory("", my_bucket):
        raise NameError("Bucket: " + my_bucket.name + '=> file does not exists: ' + name_file_source)

    # Copy object A as object B
    key_src = {"Bucket": my_bucket.name, "Key": name_file_source}
    my_bucket.meta.client.copy(Bucket=my_bucket.name, CopySource=key_src, Key=name_file_dest)

    # Delete the former object A
    my_bucket.meta.client.delete_object(Bucket=my_bucket.name, Key=name_file_source)
    return 1


def store_df_s3_data_stats(df, s3_resource, version, args, logger):
    """
    Store a dataframe in S3
    :param df: dataframe to store
    :param file_name: File name
    :param s3_resource: S3 client
    :return: "ok" if everything went fine
    """
    file_name = args.output_file
    bucket_name = args.bucket_name

    file_name = file_name if file_name.endswith('.') else file_name + '.'

    """df = df.fillna(np.nan)
    df_unique_values = pd.DataFrame()
    for col in df.columns:
        df_tmp = pd.DataFrame({col: ['Nb values: ' + str(len(df[col].unique()))] + \
                            ["Nb null: " + str(len(df[df[col] == df[col]]))] + \
                           df[col].unique().tolist()})
        df_unique_values = pd.concat([df_unique_values, df_tmp], axis=1)"""

    writer = pd.ExcelWriter(file_name + 'xlsx', engine='xlsxwriter')

    # Write each dataframe to a different worksheet.
    df.to_excel(writer, sheet_name='data')
    # df_unique_values.to_excel(writer, sheet_name='statistics')
    writer.save()

    df.to_csv(file_name + "csv")

    logger.info("Upload to AWS S3 file: " + version + '/' + file_name)
    s3_resource.meta.client.upload_file(file_name + "csv", bucket_name,
                                        args.aws_folder_intermediate + version + '/' + file_name + "csv")
    s3_resource.meta.client.upload_file(file_name + "xlsx", bucket_name,
                                        args.aws_folder_intermediate + version + '.excel/' + file_name + "xlsx")
    logger.info("Uploaded files")
    return 0

def get_verision_data(my_bucket, args, logger, create_folder):
    """

    :param path_version_file:
    :param my_bucket:
    :param args:
    :param logger:
    :return:
    """
    logger.info("Get or create data version")
    version = 0.0
    for aws_object in my_bucket.objects.filter(Prefix=args.aws_folder_intermediate):
        if str(aws_object.key).endswith('/'):
            try:
                tmp_version = float(aws_object.key.split('/')[-2])
                if tmp_version > float(version):
                    version = tmp_version
            except:
                continue

    if create_folder:
        version = version + 1
        logger.info("create folder: " + str(version))
        my_bucket.put_object(Bucket=args.bucket_name, Key=(args.aws_folder_intermediate+str(version)+'/'))
        my_bucket.put_object(Bucket=args.bucket_name, Key=(args.aws_folder_intermediate + str(version) + '.excel/'))
    else:
        logger.info("Using version: " + str(version))

    return str(version)


def get_closest_tubes_from_val_ref(df_ict_ext_round, df_od_wt_ref):
    ict_od_mm_column_name = 'ict_od_mm'
    ict_wt_mm_column_name = 'ict_awt_mm'

    od_ref_od_mm = 'ref_val_od_mm'
    wt_ref_od_mm = 'ref_val_wt_mm'

    df_ict_ext_round["map_ref_val_diameter_height"] = df_ict_ext_round[ict_od_mm_column_name]
    df_ict_ext_round["map_ref_val_wall_thickness"] = df_ict_ext_round[ict_wt_mm_column_name]

    for od_mm in df_ict_ext_round[ict_od_mm_column_name].unique():
        for wt_mm in df_ict_ext_round[(df_ict_ext_round[ict_od_mm_column_name] == od_mm)][
            ict_wt_mm_column_name].unique():
            if len(df_od_wt_ref[(df_od_wt_ref[od_ref_od_mm] == od_mm) & \
                                (df_od_wt_ref[wt_ref_od_mm] == wt_mm)]) == 0:
                list_od_ref = list(df_od_wt_ref[od_ref_od_mm].unique())
                list_wt_ref = list(df_od_wt_ref[wt_ref_od_mm].unique())

                closest_ref_number_od = min(list_od_ref, key=lambda x: abs(x - od_mm))
                closest_ref_number_wt = min(list_wt_ref, key=lambda x: abs(x - wt_mm))

                od_final = od_mm
                wt_final = wt_mm
                if abs(closest_ref_number_od - od_mm) >= abs(closest_ref_number_wt - wt_mm):
                    wt_final = closest_ref_number_wt
                    if len(df_od_wt_ref[(df_od_wt_ref[od_ref_od_mm] == od_mm) & \
                                        (df_od_wt_ref[wt_ref_od_mm] == closest_ref_number_wt)]) == 0:
                        od_final = closest_ref_number_od

                elif abs(closest_ref_number_od - od_mm) < abs(closest_ref_number_wt - wt_mm):
                    od_final = closest_ref_number_od
                    if len(df_od_wt_ref[(df_od_wt_ref[od_ref_od_mm] == closest_ref_number_od) & \
                                        (df_od_wt_ref[wt_ref_od_mm] == wt_mm)]) == 0:
                        wt_final = closest_ref_number_wt

                df_ict_ext_round.loc[(df_ict_ext_round[ict_od_mm_column_name] == od_mm) & \
                                     (df_ict_ext_round[ict_wt_mm_column_name] == wt_mm) \
                    , 'map_ref_val_diameter_height'] = od_final

                df_ict_ext_round.loc[(df_ict_ext_round[ict_od_mm_column_name] == od_mm) & \
                                     (df_ict_ext_round[ict_wt_mm_column_name] == wt_mm) \
                    , 'map_ref_val_wall_thickness'] = wt_final

    return df_ict_ext_round


def find_closest_height_width(df_od_wt_ref_square, v_list_height_ref, v_list_width_ref, ict_height
                              , ict_width, height_ref_mm, width_ref_mm):
    """Find the closest couple of width and height for one pipe

    Args:
        df_od_wt_ref_square ([DataFrame]): Vallourec square pipe reference
        v_list_height_ref ([List]): [List of height reference]
        v_list_width_ref ([List]): [List of width reference]
        ict_height ([float]): [Ict height input]
        ict_width ([float]): [Ict width input]
        height_ref_mm ([type]): [description]
        width_ref_mm ([type]): [description]

    Returns:
        [type]: [description]
    """
    closest_ref_number_height = min(v_list_height_ref, key=lambda x: abs(x - ict_height))
    closest_ref_number_width = min(v_list_width_ref, key=lambda x: abs(x - ict_width))

    if len(df_od_wt_ref_square[(df_od_wt_ref_square[height_ref_mm] == closest_ref_number_height) \
                              & (df_od_wt_ref_square[width_ref_mm] == closest_ref_number_width)]) == 0:

        if abs(closest_ref_number_height - ict_height) < abs(closest_ref_number_width - ict_width):
            if len(v_list_height_ref) > 0:
                v_list_height_ref.remove(closest_ref_number_height)
            else:
                return None, None
        else:
            if len(v_list_width_ref) > 0:
                v_list_width_ref.remove(closest_ref_number_width)
            else:
                return None, None
        closest_ref_number_height, closest_ref_number_width = find_closest_height_width(df_od_wt_ref_square
                                                                                        , v_list_height_ref, v_list_width_ref
                                                                                        , ict_height, ict_width
                                                                                        , height_ref_mm, width_ref_mm)
    return closest_ref_number_height, closest_ref_number_width


def get_closest_square_wt_tube_from_vall_ref(df_ict_ext_square, df_od_wt_ref_square, wt_column_name, logger):
    """
    Get the closest wall thickness tubes compare to the Vallourec reference:
    Args:
        df_ict_ext_square ([DataFrame]): [Input ICT DataFrame]
        df_od_wt_ref_square ([type]): [Vallourec square tubes reference]
        logger ([Logger]): [App's logger]
    """
    logger.info("get closest square wt")
    # 'ict_awt_mm'

    wt_ref_mm = 'ref_val_wt_mm'
    
    df_ict_ext_square["map_ref_val_wall_thickness"] = df_ict_ext_square[wt_column_name]

    for ict_wt in df_ict_ext_square[wt_column_name].unique():
        if ict_wt not in list(df_od_wt_ref_square[wt_ref_mm].unique()):

            list_wt_ref = list(df_od_wt_ref_square[wt_ref_mm].unique())

            closest_ref_number_wt = min(list_wt_ref, key=lambda x: abs(x - ict_wt))

            df_ict_ext_square.loc[(df_ict_ext_square[wt_column_name] == ict_wt) \
                    , 'map_ref_val_wall_thickness'] = closest_ref_number_wt
                    
    logger.info("len df_ict_ext_square: " + str(len(df_ict_ext_square)))
    return df_ict_ext_square


def get_closest_height_width_tube_from_vall_ref(df_square, df_od_wt_ref_square
                                                , height_column_name, width_mm_column_name, logger):
    """
    Get the closest square height & width compare to the Vallourec reference:
    Args:
        df_ict_ext_square ([DataFrame]): [Input ICT DataFrame]
        df_od_wt_ref_square ([DataFrame]): [Vallourec square tubes reference DataFrame]
        logger ([Logger]): [App's logger]
    """
    logger.info("Get closest square height & width")

    height_ref_mm = 'ref_val_height_mm'
    width_ref_mm = 'ref_val_width_mm'

    df_square["map_ref_val_diameter_height"] = df_square[height_column_name]
    df_square["map_ref_val_width"] = df_square[width_mm_column_name]

    for index, row in df_square[[height_column_name, width_mm_column_name]].drop_duplicates().iterrows():
        if len(df_od_wt_ref_square[(df_od_wt_ref_square[height_ref_mm] == row[height_column_name]) \
                                & (df_od_wt_ref_square[width_ref_mm] == row[width_mm_column_name])]) == 0:

            list_height_ref = list(df_od_wt_ref_square[height_ref_mm].unique())
            list_width_ref = list(df_od_wt_ref_square[width_ref_mm].unique())

            closest_ref_number_height, closest_ref_number_width = find_closest_height_width(df_od_wt_ref_square
                                                                                            , list_height_ref
                                                                                            , list_width_ref
                                                                                            , row[height_column_name]
                                                                                            , row[width_mm_column_name]
                                                                                            , height_ref_mm
                                                                                            , width_ref_mm)

            df_square.loc[(df_square[height_column_name] == row[height_column_name]) & \
                                    (df_square[width_mm_column_name] == row[width_mm_column_name]) \
                    , 'map_ref_val_diameter_height'] = closest_ref_number_height

            df_square.loc[(df_square[height_column_name] == row[height_column_name]) & \
                                    (df_square[width_mm_column_name] == row[width_mm_column_name]) \
                    , 'map_ref_val_width'] = closest_ref_number_width
    logger.info("len df_square: " + str(len(df_square)))
    return df_square



def read_grouping_tech_var_file(grouping_tech_var_file, logger):
    from openpyxl import load_workbook

    logger.info("Reading file {}".format(grouping_tech_var_file))

    xl = pd.ExcelFile(grouping_tech_var_file, engine='openpyxl')
    header_ind = 2
    df_0 = xl.parse(sheet_name=0, header=header_ind)

    wb = load_workbook(filename=grouping_tech_var_file, read_only=False)
    sheets = wb.sheetnames
    sheet_0 = wb[sheets[0]]

    # DONE fixed merged cells bug
    for e in sheet_0.merged_cells:
        c1, r1, c2, r2 = e.bounds
        top_value = sheet_0.cell(r1, c1).value
        df_0.iloc[r1 - header_ind - 1:r2 - header_ind - 1, c1 - 1] = top_value

    logger.info(df_0['Unnamed: 5'].dropna())

    df_1 = xl.parse(sheet_name=1, header=header_ind)
    sheet_1 = wb[sheets[1]]

    for e in sheet_1.merged_cells:
        c1, r1, c2, r2 = e.bounds
        top_value = sheet_1.cell(r1, c1).value
        df_1.iloc[r1 - header_ind - 1:r2 - header_ind - 1, c1 - 1] = top_value

    # DONE rename all the df_0 and df_1 columns
    df_0 = df_0.rename(columns={'Length type': 'yfb_length_type',
                                'Ultrasound Name': 'yfb_ultrasound_type',
                                'Heat Treatment Name': 'yfb_heat_treatment',
                                'Ext. Corner Prof. Name (B)': 'yfb_ext_corner_radius',
                                'Curvature Name (B)': 'yfb_curvature_tolerance'}).dropna(how='all', axis=1).dropna(
        how='all').reset_index(drop=True)

    logger.info("df_0 columns are {}".format(df_0.columns))

    df_1 = df_1.rename(columns={'Leading Norm 3 Name (B)': "yfb_norm_3"
        , 'Leading Norm 2 Name (B)': 'yfb_norm_2'
        , "Leading Norm 1 Name (B)": "yfb_norm_1"
        , "Grade 1 Norm Name (B)": "yfb_grade_1"
        , "Grade 2 Norm Name (B)": "yfb_grade_2"
        , "Grade 3 Norm Name (B)": "yfb_grade_3"}).dropna(how='all', axis=1).dropna(how='all').reset_index(drop=True)

    logger.info("df_1 columns are {}".format(df_1.columns))

    # DONE Concat keeping all the df_0 and df_1 columns
    df_dico = pd.concat([df_0, df_1], axis=1)[['yfb_length_type', 'Group with',
                                               'yfb_heat_treatment', 'Group with.1', 'yfb_ultrasound_type',
                                               'Group with.2',
                                               'yfb_ext_corner_radius', 'Group with.3', 'yfb_curvature_tolerance',
                                               'Group with ', 'yfb_norm_3', 'Grouping', 'yfb_norm_2', 'Grouping.1',
                                               'yfb_norm_1', 'Grouping.2', 'yfb_grade_3', 'Grouping.3', 'yfb_grade_2',
                                               'Grouping.4', 'yfb_grade_1', 'Grouping.5']]

    logger.info("df_dico columns are {}".format(df_dico.columns))
    logger.info("df_dico columns number {}".format(len(df_dico.columns)))

    dico = {}
    for i in range(int(len(df_dico.columns) / 2)):
        dico_col = {}
        for ind, val in df_dico.iloc[:, 2 * i + 1].dropna().items():
            dico_col[df_dico.iloc[ind, 2 * i].strip().upper()] = val.strip().upper()

        dico[df_dico.columns[int(2 * i)]] = dico_col

    return dico
