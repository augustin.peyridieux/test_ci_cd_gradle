import requests
import urllib
import pandas as pd
import json
from pandas.io.json import json_normalize
import itertools
import re


class SharepointApi:
    """
    Class used to request Sharepoint API
    Classe pour requêter l'API sharepoint

    TODO: Faire la gestion de l'expiration de token
    TODO: Compléter les requêtes
    TODO: faire la gestion des erreurs
    """

    def __init__(self, host, site, client_secret, client_id, tenant_id, url_access="", principal=""):
        # Déclaration des variables sharepoint
        # Nom de domaine
        self.host = host
        # Nom du domaine pour récupérer le token (sera toujours le même)
        if not url_access:
            self.url_access = "accounts.accesscontrol.windows.net/"
        else:
            self.url_access = url_access

        # Nom de la team / site / autre où les fichiers ou autres seront stockés (visible dans l'url quand dans le dossier sharepoint)
        self.site = site

        # Client secret à demander à l'administrateur Office365
        self.client_secret = client_secret

        # Client ID à demander à l'administrateur Office365
        self.client_id = client_id

        # Tenant id à demander à l'administrateur Office365
        self.tenant_id = tenant_id

        # Principal qui ne change pas (à vérifier)
        if not principal:
            self.principal = "00000003-0000-0ff1-ce00-000000000000"
        else:
            self.principal = principal

        # Url principale
        self.api_url = "https://" + self.host + "/teams/" + self.site + "/_api/"

        # Url de demande de token
        self.api_url_token = "https://" + self.url_access + self.tenant_id + "/tokens/OAuth/2"

        # Token d'identification
        self.get_token()

    def get_token(self):
        """
        Get identification token

        :return:
        """
        payload = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data;" + \
                  "name=\"grant_type\"\r\n\r\nclient_credentials\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition:" + \
                  "form-data; name=\"client_id\"\r\n\r\n" + self.client_id + "@" + self.tenant_id + "\r\n" + \
                  "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data;" + \
                  "name=\"client_secret\"\r\n\r\n" + self.client_secret + "\r\n" + \
                  "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data;" + \
                  "name=\"resource\"\r\n\r\n" + self.principal + "/" + self.host + \
                  "@" + self.tenant_id + "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--"
        headers = {
            'content-type': "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
            'cache-control': "no-cache"
        }
        response = requests.request("POST", self.api_url_token, data=payload, headers=headers)
        self.token = response.json()['access_token']
        return 1  # TODO a améliorer

    def get_request_digest(self):
        """
        Get a valid request digest

        :return: String, a request digest
        """
        headers = {
            'Authorization': "Bearer " + self.token,
            'Accept': "application/json;odata=nometadata",
            'cache-control': "no-cache",
        }
        r = requests.post(self.api_url + "contextinfo", headers=headers, verify=False)
        if r.status_code == 200:
            return r.json()["FormDigestValue"]
        else:
            print('An error has occurred when you are trying to get the value of X-RequestDigest. \n')
            return 1

    def get_list_files_from_folder(self, path_folder):
        """
        List all of files inside the folder
        :param path_folder: String, folder path
        :return: Dict, a dict that contains all files inside the folder
        """
        path = urllib.parse.quote("web/GetFolderByServerRelativeUrl('" + path_folder + "')/Files", safe='')

        url = self.api_url + path

        headers = {
            'Authorization': "Bearer " + self.token,
            'Accept': "application/json;odata=nometadata",
            'cache-control': "no-cache",
        }

        return requests.request("GET", url, headers=headers).json()

    def downloadFileFromSharepoint(self, foldername, filename):
        """

        :param foldername: String, Name of the folder
        :param filename: String, name of the file
        :return:
        """
        path = urllib.parse.quote("web/GetFolderByServerRelativeUrl('" + foldername + "')", safe='')

        file_url = urllib.parse.quote("/Files('" + filename + "')/$value", safe='')

        url = self.api_url + path + file_url

        headers = {
            'Authorization': "Bearer " + self.token,
            'Accept': "application/json;odata=nometadata",
            'cache-control': "no-cache",
        }

        r = requests.request("GET", url, headers=headers, stream=True)
        with open(filename, 'wb+') as f:
            for chunk in r.iter_content(chunk_size=1024):
                if chunk:
                    f.write(chunk)
                    f.flush()
        return 1  # TODO: A améliorer

    def get_metadata_list_sharepoint(self, list_name):
        """

        :param list_name: String, name of the list Sharepoint
        :return: Response
        """
        # http://server/site/_api/lists/getbytitle('listname')
        path = urllib.parse.quote("lists/getbytitle('" + list_name + "')", safe='')
        url = self.api_url + path
        headers = {
            'Authorization': "Bearer " + self.token,
            'Accept': "application/json;odata=nometadata",
            'cache-control': "no-cache",
        }

        r = requests.request("GET", url, headers=headers, stream=True)

        return r  # TODO: A améliorer

    def get_list_sharepoint(self, list_name):
        """
        Get all items in the list Sharepoint
        :param list_name: String, name of the list Sharepoint
        :return: pd.DataFrame that contains all items in the list
        """
        r = self.get_metadata_list_sharepoint(list_name)
        print('List size: ' + str(r.json()['ItemCount']) + ' rows')
        print('Depending the charge on server side it will take approximately ' + \
              str(int(int(r.json()['ItemCount'] * 0.005666666666666667) / 60)) + 'min to pull the full list')
        path = urllib.parse.quote("lists/getbytitle('" + list_name + "')/items", safe='') + "?$skiptoken=Paged%3dTRUE"
        url = self.api_url + path
        headers = {
            'Authorization': "Bearer " + self.token,
            'Accept': "application/json;odata=nometadata",
            'cache-control': "no-cache",
        }

        to_continue = True
        df = pd.DataFrame()
        # When the list is empty return a dataFrame empty
        if r.json()['ItemCount'] == 0:
            return df
        while to_continue:
            r = requests.request("GET", url, headers=headers, stream=True)
            data = json.loads(r.text)
            df = df.append(json_normalize(data['value']))

            try:
                url = r.json()['odata.nextLink']
            except:
                to_continue = False
        return df

    def get_all_lists(self):
        """
        Get all lists of sharepoints
        :return: Response
        """
        path = "lists"
        url = self.api_url + path
        headers = {
            'Authorization': "Bearer " + self.token,
            'Accept': "application/json;odata=nometadata",
            'cache-control': "no-cache",
        }
        r = requests.request("GET", url, headers=headers, stream=True)
        return r

    def list_all_files(self, url_folder, r=None):
        """
        Get all files with name, relativeURL and IDs in the url folder (files in subdirectories included)
        :param url_folder: String, folder path
        :param r: List, it will contains the all file and sub files in the folder with their UniqueId, Name and
        ServerRelativeUrl
        :return: List of dict, each dict contains a Name, ServerRelativeUrl and UniqueID of a file
        """
        if r is None:
            r = []
        if not(url_folder.startswith("/")):
            url_folder = "/" + url_folder

        path = urllib.parse.quote("web/GetFolderByServerRelativeUrl('Shared%20Documents" + url_folder + "')",
                                  safe='') + "?$expand=Folders,Files"

        url = self.api_url + path
        headers = {
            'Authorization': "Bearer " + self.token,
            'Accept': "application/json;odata=nometadata",
            'cache-control': "no-cache",
        }
        res = requests.request("GET", url, headers=headers).json()

        if len(res.get("Folders")):
            if len(res.get("Files")):
                r.append([{"UniqueId": f.get("UniqueId"), "Name": f.get("Name"),
                           "ServerRelativeUrl": f.get("ServerRelativeUrl")} for f in res.get("Files")])

            for f in res.get("Folders"):
                self.list_all_files(f.get("ServerRelativeUrl").replace("/teams/" + self.site + "/Shared Documents", ""), r)

        else:
            if len(res.get("Files")):
                r.append([{"UniqueId": f.get("UniqueId"), "Name": f.get("Name"),
                           "ServerRelativeUrl": f.get("ServerRelativeUrl")} for f in res.get("Files")])

        r = list(itertools.chain(*r))
        return r

    def get_file_from_id(self, file_id, logging):
        """
        Get the content of the file in bytes format
        :param file_id: String, Unique ID of a specific file
        :return: content in bytes
        """
        path = urllib.parse.quote("web/GetFileById('" + file_id + "')", safe='') + "/$value"
        url = self.api_url + path
        headers = {
            'Authorization': "Bearer " + self.token,
            'Accept': "application/json;odata=nometadata",
            'cache-control': "no-cache",
        }
        logging.info("Get file content of {0}".format(url))
        res = requests.request("GET", url, headers=headers)
        if res.status_code != 200:
            logging.info("Response status:{0}, Response reason:{1}".format(res.status_code, res.reason))
        return res.content

    def get_list_item_all_fields(self, file_id, logging):
        """
        Gets a value that specifies the list item field values for the list item corresponding to the file.
        :param file_id: String, Unique ID of a specific file
        :return: Dict, a dict of all fields of a specific file
        """
        path = urllib.parse.quote("web/GetFileById('" + file_id + "')/ListItemAllFields", safe='')
        url = self.api_url + path
        headers = {
            'Authorization': "Bearer " + self.token,
            'Accept': "application/json;odata=verbose",
            'cache-control': "no-cache",
        }
        logging.info("Get all information about {0}".format(url))
        res = requests.request("GET", url, headers=headers)
        if res.status_code != 200:
            logging.info("Response status:{0}, Response reason:{1}".format(res.status_code, res.reason))
        return res.json()

    def copy_file_to_hdfs(self, folder_dst_hdfs, name_file, content_file, client_hdfs):
        """
        Copy a file in Sharepoint to HDFS
        :param folder_dst_hdfs: String, HDFS destination path
        :param name_file: String, file name in HDFS
        :param content_file: bytes, file content
        :param client_hdfs: ClientHDFS
        :return:
        """
        with client_hdfs.write(folder_dst_hdfs + name_file, overwrite=True) as writer:
            writer.write(content_file)

    def transfer_sp_to_hfs(self, folder_dst_hdfs, sp_info, client_hdfs):
        """

        :param folder_dst_hdfs:String, HDFS destination folder
        :param sp_info: Dict, contains UniqueId, Name and ServerRelativeUrl of a file (eg: an item of the result of the function
        list_all_files)
        :param client_hdfs:ClientHDFS
        :return:
        """
        m = re.search("/teams/" + self.site + "/Shared Documents.+/", sp_info.get("ServerRelativeUrl"))
        project_name = m.group(0).replace("/teams/" + self.site + "/Shared Documents", "")
        file_dst_hdfs = folder_dst_hdfs + project_name
        content_file = self.get_file_from_id(sp_info.get("UniqueId"))
        self.copy_file_to_hdfs(file_dst_hdfs, sp_info.get("Name"), content_file, client_hdfs)

    def update_file_field(self, file_id, field_name, new_field_value, logging):
        """
        Update the field with the new value
        :param file_id: String, Unique ID of the file
        :param field_name: String, Name of the field that we want to update value
        :param new_field_value: String, new value of the field
        :return: Response
        """
        path = urllib.parse.quote("web/GetFileById('" + file_id + "')/ListItemAllFields", safe='')
        url = self.api_url + path
        file_all_fields = self.get_list_item_all_fields(file_id).get("d")
        type_metadata = file_all_fields.get("__metadata").get("type")
        field_name = field_name.replace(" ", "_x0020_")
        if field_name not in file_all_fields.keys():
            logging.warning("Field name is not valid. Please verify your input")
            return 1
        payload = {"__metadata": {"type": type_metadata}, field_name: new_field_value}
        headers = {
            'Authorization': "Bearer " + self.token,
            "X-RequestDigest": self.get_request_digest(),
            "IF-MATCH": "*",
            "X-HTTP-Method": "MERGE",
            "Accept": "application/json; odata=verbose",
            "content-Type": "application/json; odata=verbose",
            'cache-control': "no-cache"
        }
        logging.info("Update the value of the field {0} to {1}".format(field_name, new_field_value))
        res = requests.post(url, headers=headers, data=json.dumps(payload))
        if res.status_code != 204:
            logging.info("Response status:{0}, Response reason:{1}".format(res.status_code, res.reason))
        return res

    def get_file_field(self, file_id, field_name, logging):
        """
        Get the field value of a specific file
        :param file_id: String, Unique ID of the file
        :param field_name: String, Name of the field that we want to get value
        :return: String, value of the field
        """
        file_all_fields = self.get_list_item_all_fields(file_id).get("d")
        field_name = field_name.replace(" ", "_x0020_")
        if field_name not in file_all_fields.keys():
            logging.warning("Field name is not valid. Please verify your input")
            return 1
        res = file_all_fields.get(field_name)
        return res

    def upload_file(self, relative_url, filename):
        with open(filename, 'rb') as d:
            content_file = d.read()
        self.upload_file_data(relative_url, filename, content_file)

    def upload_file_data(self, relative_url, filename, content_file):
        """
        Upload file in Sharepoint
        :param relative_url: String,
        :param filename: String,
        :param content_file: Bytes,
        :return:
        """
        path = urllib.parse.quote(
            "web/GetFolderByServerRelativeUrl('" + relative_url + "')", safe='')

        file_url = urllib.parse.quote("/files/add(overwrite=true, url='" + filename + "')", safe='')

        url = self.api_url + path + file_url

        headers = {
            'Authorization': "Bearer " + self.token,
            "X-RequestDigest": self.get_request_digest(),
            "Accept": "application/json; odata=verbose",
            'cache-control': "no-cache"
        }
        r = requests.request("POST", url, headers=headers, data=content_file)
        if r.status_code != 200:
            print("Response status:{0}, Response reason:{1}".format(r.status_code, r.reason))
        return r
