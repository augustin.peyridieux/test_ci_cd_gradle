import sys
sys.path.append("utils/")
sys.path.append("../dependencies/utils/")
sys.path.append("pricing_project_industry-master/code/dependencies/utils/")
import utils
import common_business_rules as cbr
import utils_merg_yf_ict_vntu
import boto3
import sys

logger = utils.create_logger('PPI: Join Yellowfin ICT files')

logger.info("Starting: " + str(sys.argv[0]))
args = utils_merg_yf_ict_vntu.get_parameters(logger)

s3_resource = boto3.resource('s3')
my_bucket = s3_resource.Bucket(name=args.bucket_name)

version = utils.get_verision_data(my_bucket, args, logger, False)

utils_merg_yf_ict_vntu.download_aws_files(my_bucket, args, logger)

df_yellowfin_bookings = utils_merg_yf_ict_vntu.read_yellowfin_bookings(args.yellowfin_bookings_file, cbr.list_country_eu_geo, logger)
df_yellowfin_costs = utils_merg_yf_ict_vntu.read_yellowfin_costs(args.yellowfin_costs_file, logger)

df_map_enduser = utils_merg_yf_ict_vntu.read_map_enduser_file(args.map_enduser_file, logger)
df_map_market_segment = utils_merg_yf_ict_vntu.read_map_market_segment_file(args.map_marketing_segment_file, logger)
df_map_order_file = utils_merg_yf_ict_vntu.read_map_orderer_file(args.map_orderer_file, logger)
df_mapping_mill_grade = utils_merg_yf_ict_vntu.read_map_grade_file(args.map_mill_grade_file, logger)
# df_mapping_mill_grade_compet_val = utils.strip_df_col_values(pd.read_excel(args.map_od_compte_od_val))

df_od_wt_ref = utils_merg_yf_ict_vntu.read_ref_file(args.od_wt_ref_file, logger)
df_ict_ext_round = utils_merg_yf_ict_vntu.read_ict_round(args.ict_round_file, cbr.list_country_eu_geo, logger)
df_ict_ext_round = utils_merg_yf_ict_vntu.treat_ict(df_ict_ext_round, df_od_wt_ref, 'round', logger)
df_ict_ext_round = df_ict_ext_round[df_ict_ext_round['ict_manufacturer_group'] == 'Vallourec']
df_ict_ext_round = utils_merg_yf_ict_vntu.split_ict_commodity(df_ict_ext_round, logger)

df_square_ref = utils_merg_yf_ict_vntu.read_ref_file(args.od_wt_ref_file_square, logger)
df_ict_ext_square = utils_merg_yf_ict_vntu.read_ict_square(args.ict_square_file, cbr.list_country_eu_geo, logger)
df_ict_ext_square = utils_merg_yf_ict_vntu.treat_ict(df_ict_ext_square, df_square_ref, 'square', logger)
df_ict_ext_square = df_ict_ext_square[df_ict_ext_square['ict_manufacturer_group'] == 'Vallourec']
df_ict_ext_square = utils_merg_yf_ict_vntu.split_ict_commodity(df_ict_ext_square, logger)

df_mapping_mills_name = utils_merg_yf_ict_vntu.read_map_mill_name(args.map_mill_ict_yellow_file, logger)

#VNTU no longer used
#df_vntu_price_list = utils_merg_yf_ict_vntu.read_vntu_price_list_file(args.vntu_price_list_file, logger)

df_merged_yellowfin = utils_merg_yf_ict_vntu.merge_yellowfin_map(args.grouping_tech_var_file, df_yellowfin_bookings
                                                                 , df_yellowfin_costs, df_map_order_file
                                                                 , df_map_market_segment, df_map_enduser
                                                                 , df_mapping_mill_grade, df_mapping_mills_name, logger)



# Split DataFrames by square types to treat them separately
df_merged_yellowfin_round = df_merged_yellowfin[df_merged_yellowfin['calc_pipe_type'] == 'round']
df_merged_yellowfin_square = df_merged_yellowfin[df_merged_yellowfin['calc_pipe_type'] == 'square']
df_merged_yellowfin_others = df_merged_yellowfin[~df_merged_yellowfin['calc_pipe_type'].isin(['round', 'square'])]

# Harmonizing round tubes with Vallourec reference
df_merged_yellowfin_round = utils_merg_yf_ict_vntu.get_yellowfin_ref_od_wt(df_merged_yellowfin_round, df_od_wt_ref, logger)
df_merged_yellowfin_round = utils_merg_yf_ict_vntu.calc_diff_od_wt(df_merged_yellowfin_round, logger)

# Harmonizing square tubes with Vallourec reference
df_merged_yellowfin_square = utils.get_closest_height_width_tube_from_vall_ref(df_merged_yellowfin_square, df_square_ref, 'yfb_diameter_height', 'yfb_width', logger)
df_merged_yellowfin_square = utils.get_closest_square_wt_tube_from_vall_ref(df_merged_yellowfin_square, df_square_ref, 'yfb_wall_thickness', logger)


#VNTU no longer used
#df_merged_yellowfin = utils_merg_yf_ict_vntu.merge_vntu(df_merged_yellowfin, df_vntu_price_list, logger)

# Merge ICT and Yellowfin
df_merged_yellowfin = utils_merg_yf_ict_vntu.merge_ict_yellowfin(df_ict_ext_round, df_ict_ext_square
                                                       , df_merged_yellowfin_round, df_merged_yellowfin_square
                                                       , df_merged_yellowfin_others, logger)

df_merged_yellowfin = utils_merg_yf_ict_vntu.calc_columns(df_merged_yellowfin, logger)

df_merged_yellowfin = utils_merg_yf_ict_vntu.order_columns(df_merged_yellowfin, logger)

logger.info("taille DataFrame: " + str(len(df_merged_yellowfin)))
logger.info("Storing files in version: " + str(version))
utils.store_df_s3_data_stats(df_merged_yellowfin, s3_resource, version, args, logger)