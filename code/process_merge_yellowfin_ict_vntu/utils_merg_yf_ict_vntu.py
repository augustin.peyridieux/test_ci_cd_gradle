import sys
sys.path.append("utils/")
sys.path.append("../dependencies/utils/")
sys.path.append("pricing_project_industry-master/code/dependencies/utils/")
import utils
import common_business_rules as cbr
import argparse
import pandas as pd
import numpy as np
from datetime import datetime
import re

def get_parameters(logger):
    """

    :param logger:
    :return:
    """
    logger.info("Getting parameters")
    desc = "Pricing Project Industry: Merge ICT and Yellowfin files"

    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("bucket_name",
                        help="Name of the AWS S3 bucket name")
    parser.add_argument("aws_folder_raw",
                        help=("Name of the AWS S3 folder raw"))
    parser.add_argument("aws_folder_intermediate",
                        help=("Name of the AWS S3 folder intermediate"))
    parser.add_argument("aws_folder_mapping",
                        help=("Name of the AWS S3 folder intermediate"))
    parser.add_argument("ict_round_file",
                        help=("Name of the ICT round file"))
    parser.add_argument("ict_square_file",
                        help=("Name of the ICT square file"))
    parser.add_argument("yellowfin_bookings_file",
                        help=("Name of the Yellowfin bookings file"))
    parser.add_argument("yellowfin_costs_file",
                        help=("Name of the Yellowfin costs file"))
    #parser.add_argument("yellowfin_invoices_file",
    #                    help=("Name of the Yellowfin invoices file"))
    parser.add_argument("map_mill_grade_file",
                        help=("Name of the mapping Mill Grade file"))
    parser.add_argument("map_mill_ict_yellow_file",
                        help=("Name of the mapping Millsi ICT YellowFin file"))
    parser.add_argument("map_marketing_segment_file",
                        help=("Name of the marketing segment file"))
    parser.add_argument("map_orderer_file",
                        help=("Name of the orderer mapping file"))
    parser.add_argument("grouping_tech_var_file",
                        help=("Name of the technical variable mapping file"))
    parser.add_argument("map_enduser_file",
                        help=("Name of the enduser mapping file"))
    #VNTU no longer needed
    #parser.add_argument("vntu_price_list_file",
    #                    help=("Name of the VNTU price list file"))
    parser.add_argument("od_wt_ref_file",
                        help=("Name of the OD WT referential file"))
    parser.add_argument("od_wt_ref_file_square",
                        help=("Name of the width height referential square file"))
    parser.add_argument("map_od_compte_od_val",
                        help=("Name of the OD WT referential file"))
    parser.add_argument("output_file",
                        help=("Name of the output file"))

    args = parser.parse_args()

    args.aws_folder_raw = utils.add_backslash(args.aws_folder_raw)
    args.aws_folder_intermediate = utils.add_backslash(args.aws_folder_intermediate)
    args.aws_folder_mapping = utils.add_backslash(args.aws_folder_mapping)

    logger.info("bucket_name: " + str(args.bucket_name))
    logger.info("aws_folder_raw: " + str(args.aws_folder_raw))
    logger.info("aws_folder_intermediate: " + str(args.aws_folder_intermediate))
    logger.info("aws_folder_mapping: " + str(args.aws_folder_mapping))
    logger.info("ict_round_file: " + str(args.ict_round_file))
    logger.info("ict_square_file: " + str(args.ict_square_file))
    logger.info("yellowfin_bookings_file: " + str(args.yellowfin_bookings_file))
    logger.info("yellowfin_costs_file: " + str(args.yellowfin_costs_file))
    #logger.info("yellowfin_invoices_file: " + str(args.yellowfin_invoices_file))
    logger.info("map_mill_grade_file: " + str(args.map_mill_grade_file))
    logger.info("map_mill_ict_yellow_file: " + str(args.map_mill_ict_yellow_file))
    logger.info("map_marketing_segment_file: " + str(args.map_marketing_segment_file))
    logger.info("map_orderer_file: " + str(args.map_orderer_file))
    logger.info("grouping_tech_var_file: " + str(args.grouping_tech_var_file))
    logger.info("map_enduser_file: " + str(args.map_enduser_file))
    #logger.info("vntu_price_list_file: " + str(args.vntu_price_list_file))
    logger.info("od_wt_ref_file: " + str(args.od_wt_ref_file))
    logger.info("od_wt_ref_file_square: " + str(args.od_wt_ref_file_square))
    logger.info("map_od_compte_od_val: " + str(args.map_od_compte_od_val))
    logger.info("output_file: " + str(args.output_file))

    return args


def download_aws_files(my_bucket, args, logger):
    """

    :param my_bucket:
    :param args:
    :param logger:
    :return:
    """
    logger.info("Download AWS file: " + args.aws_folder_raw + args.map_mill_grade_file)
    my_bucket.download_file(args.aws_folder_raw + args.map_mill_grade_file, args.map_mill_grade_file)

    logger.info("Download AWS file: " + args.aws_folder_mapping + args.map_mill_ict_yellow_file)
    my_bucket.download_file(args.aws_folder_mapping + args.map_mill_ict_yellow_file, args.map_mill_ict_yellow_file)

    logger.info("Download AWS file: " + args.aws_folder_intermediate + args.ict_round_file)
    my_bucket.download_file(args.aws_folder_intermediate + args.ict_round_file, args.ict_round_file)

    logger.info("Download AWS file: " + args.aws_folder_intermediate + args.ict_square_file)
    my_bucket.download_file(args.aws_folder_intermediate + args.ict_square_file, args.ict_square_file)

    logger.info("Download AWS file: " + args.aws_folder_raw + args.yellowfin_bookings_file)
    my_bucket.download_file(args.aws_folder_raw + args.yellowfin_bookings_file, args.yellowfin_bookings_file)

    logger.info("Download AWS file: " + args.aws_folder_raw + args.yellowfin_costs_file)
    my_bucket.download_file(args.aws_folder_raw + args.yellowfin_costs_file, args.yellowfin_costs_file)

    # logger.info("Download AWS file: " + args.aws_folder_raw + args.yellowfin_invoices_file)
    # my_bucket.download_file(args.aws_folder_raw + args.yellowfin_invoices_file, args.yellowfin_invoices_file)

    logger.info("Download AWS file: " + args.aws_folder_raw + args.map_enduser_file)
    my_bucket.download_file(args.aws_folder_raw + args.map_enduser_file, args.map_enduser_file)

    logger.info("Download AWS file: " + args.aws_folder_raw + args.map_marketing_segment_file)
    my_bucket.download_file(args.aws_folder_raw + args.map_marketing_segment_file, args.map_marketing_segment_file)

    logger.info("Download AWS file: " + args.aws_folder_raw + args.map_orderer_file)
    my_bucket.download_file(args.aws_folder_raw + args.map_orderer_file, args.map_orderer_file)

    logger.info("Download AWS file: " + args.aws_folder_raw + args.grouping_tech_var_file)
    my_bucket.download_file(args.aws_folder_raw + args.grouping_tech_var_file, args.grouping_tech_var_file)

    logger.info("Download AWS file: " + args.aws_folder_raw + args.od_wt_ref_file)
    my_bucket.download_file(args.aws_folder_raw + args.od_wt_ref_file, args.od_wt_ref_file)

    logger.info("Download AWS file: " + args.aws_folder_raw + args.od_wt_ref_file_square)
    my_bucket.download_file(args.aws_folder_raw + args.od_wt_ref_file_square, args.od_wt_ref_file_square)

    #logger.info("Download AWS file: " + args.aws_folder_raw + args.vntu_price_list_file)
    #my_bucket.download_file(args.aws_folder_raw + args.vntu_price_list_file, args.vntu_price_list_file)

    logger.info("Download AWS file: " + args.aws_folder_raw + args.map_od_compte_od_val)
    my_bucket.download_file(args.aws_folder_raw + args.map_od_compte_od_val, args.map_od_compte_od_val)
    return 0


def read_yellowfin_bookings(filename, list_country_eu_geo, logger):
    """

    :param filename:
    :param list_country_eu_geo:
    :param logger:
    :return:
    """
    logger.info("Read Yellowfin bookings file: " + filename)
    df_yellowfin_bookings = pd.read_csv(filename, thousands='.', decimal=',')

    df_yellowfin_bookings.columns = df_yellowfin_bookings.columns.str.strip()

    logger.info("df_yellowfin_bookings length (start read_yellowfin_bookings) {}".format(len(df_yellowfin_bookings)))
    logger.info(
        "read_yellowfin_bookings na : {}".format(len(df_yellowfin_bookings[df_yellowfin_bookings["Order"].isna()])))

    df_yellowfin_bookings = \
    utils.strip_df_col_values(df_yellowfin_bookings) \
        [['Year of Creation', 'Date of first intake', 'Order', 'Item', "Delivery Plant"  # , 'order item'
        , 'Grade', 'Diameter', 'Grade Segmentation (B)', 'Dimension 3', 'Wall thickness', 'Length type', 'Length min.',
          'Length max.'
        , 'Country enduser (B)', 'Enduser', 'Consignee', 'Country orderer', 'Orderer'
        , 'Commodity Code (B)', 'Bonus type Text (B)', 'tons booked', 'ZPVN EUR/T'
        , 'Twist Name (B)', 'Squareness Value (B)', 'Straightness Name (B)', 'Certificate Name (B)', 'NDT Type Name (B)'
        , 'Leading Norm 3 Name (B)', 'Leading Norm 2 Name (B)', 'Leading Norm 1 Name (B)', 'Grade 3 Norm Name (B)'
        , 'Grade 2 Norm Name (B)', 'Grade 1 Norm Name (B)', 'Ext. Corner Prof. Name (B)', 'Ends Name (B)',
          'Curvature Name (B)'
        , 'Ultrasound Name', 'Heat Treatment Name', 'PPP Segment Display (B)', 'Incoterm Code (B)',
          'Incoterm Destination (B)'
        , 'Enduser Text 1 (B)', 'Dim. Type Name (B)']] \
        .rename(
        columns={"Year of Creation": "yfb_year_creation",
                 "Date of first intake": "yfb_date_creation",
                 "Order": "yfb_order_number",
                 "Item": "yfb_item_number",
                 "Twist Name (B)": "yfb_twist_tolerance",
                 "Squareness Value (B)": "yfb_squareness_tolerance",
                 "Straightness Name (B)": "yfb_straightness_tolerance",
                 "Certificate Name (B)": "yfb_inspection_certificate",
                 "NDT Type Name (B)": "yfb_ndt_type",
                 "Leading Norm 3 Name (B)": "yfb_norm_3",
                 "Leading Norm 2 Name (B)": "yfb_norm_2",
                 "Leading Norm 1 Name (B)": "yfb_norm_1",
                 "Grade 3 Norm Name (B)": "yfb_grade_3",
                 "Grade 2 Norm Name (B)": "yfb_grade_2",
                 "Grade 1 Norm Name (B)": "yfb_grade_1",
                 "Ext. Corner Prof. Name (B)": "yfb_ext_corner_radius",
                 "Ends Name (B)": "yfb_pipe_end_type",
                 "Curvature Name (B)": "yfb_curvature_tolerance",
                 "Ultrasound Name": "yfb_ultrasound_type",
                 "Heat Treatment Name": "yfb_heat_treatment",
                 "PPP Segment Display (B)": "yfb_product_segment",
                 "Country orderer": "yfb_orderer_country",
                 "Incoterm Code (B)": "yfb_incoterm",
                 "Incoterm Destination (B)": "yfb_incoterm_destination",
                 "Orderer": "yfb_orderer_name",
                 "Country enduser (B)": "yfb_enduser_country",
                 "Enduser": "yfb_enduser_name",
                 "Consignee": "yfb_consignee",
                 "Grade": "yfb_internal_grade_name",
                 "Diameter": "yfb_diameter_height",
                 "Dimension 3": "yfb_width",
                 "Wall thickness": "yfb_wall_thickness",
                 "Commodity Code (B)": "yfb_import_export_code",
                 "Length type": "yfb_length_type",
                 "Length min.": "yfb_length_min",
                 "Length max.": "yfb_length_max",
                 "Bonus type Text (B)": "yfb_yearly_bonus",
                 "tons booked": "yfb_tons_booked_per_item",
                 "ZPVN EUR/T": "yfb_net_price",
                 "Grade Segmentation (B)": 'yfb_grade_segmentation_tmp',
                 "Enduser Text 1 (B)": "yfb_enduser_cpy_group_tmp",
                 "Delivery Plant": 'yfb_delivery_plant_tmp',
                 "Dim. Type Name (B)": 'yfb_dim_type_name_tmp'})

    df_yellowfin_bookings.columns = df_yellowfin_bookings.columns.str.replace('(', '').str.replace(')', '') \
        .str.replace(' B', '').str.strip()

    # Applied filters
    df_yellowfin_bookings = cbr.filter_year_of_creation(df_yellowfin_bookings)
    df_yellowfin_bookings = cbr.filter_non_numerical_order_number(df_yellowfin_bookings, 'yfb_order_number', logger)
    df_yellowfin_bookings = cbr.filter_mills(df_yellowfin_bookings, 'yfb_delivery_plant_tmp')
    df_yellowfin_bookings = cbr.filter_non_relevant_order_number(df_yellowfin_bookings)
    df_yellowfin_bookings = cbr.filter_eu_countries(df_yellowfin_bookings)
    df_yellowfin_bookings = cbr.filter_high_diameter_pipes(df_yellowfin_bookings)
    df_yellowfin_bookings = cbr.filter_zero_diameter_or_wt(df_yellowfin_bookings)

    df_yellowfin_bookings['yfb_order_number'] = df_yellowfin_bookings['yfb_order_number'].astype(int)
    df_yellowfin_bookings['yfb_item_number'] = df_yellowfin_bookings['yfb_item_number'].astype(int)

    df_yellowfin_bookings = cbr.create_column_order_item(df_yellowfin_bookings)

    df_yellowfin_bookings = cbr.calculate_inside_diameter(df_yellowfin_bookings)
    df_yellowfin_bookings['calc_pipe_type'] = df_yellowfin_bookings.apply(lambda row: cbr.calc_pipe_type(row), axis=1)
    df_yellowfin_bookings = utils.to_lowercase(df_yellowfin_bookings, 'yfb_grade_segmentation_tmp')
    df_yellowfin_bookings = utils.to_lowercase(df_yellowfin_bookings, 'yfb_internal_grade_name')

    logger.info("df_yellowfin_bookings length (end read_yellowfin_bookings) {}".format(len(df_yellowfin_bookings)))

    """
    df_yellowfin_bookings_round = utils.to_lowercase(df_yellowfin_bookings_round, 'map_grade_class')
    df_yellowfin_bookings_square = utils.to_lowercase(df_yellowfin_bookings_square, 'map_grade_class')
    df_yellowfin_bookings_others = utils.to_lowercase(df_yellowfin_bookings_others, 'map_grade_class')
    """

    return df_yellowfin_bookings


def read_yellowfin_costs(filename, logger):
    logger.info("Read Yellowfin costs file: " + filename)
    df_yellowfin_costs = pd.read_csv(filename, thousands='.', decimal=',')

    df_yellowfin_costs.columns = df_yellowfin_costs.columns.str.strip()

    logger.info("df_yellowfin_costs length (start read_yellowfin_costs) {}".format(len(df_yellowfin_costs)))
    logger.info("read_yellowfin_costs na : {}".format(len(df_yellowfin_costs[df_yellowfin_costs["Order"].isna()])))

    # args.yellowfin_costs_file
    df_yellowfin_costs = utils.strip_df_col_values(df_yellowfin_costs) \
        [[  # 'Order- Item (B)',
            'MTP Euro/t', 'MTP fix Euro/t', 'MTP Variable Costs EUR/T calc.'
            , 'Full Cost Euro/t', 'Metal with yield&scrap Euro/t', 'tons booked'
            , 'Euro/t', 'Freight provision Euro/t', "Bruttopreis EUR/T(AE)"
            , 'Order', 'Item'
        ]] \
        .rename(columns=
                {"MTP Euro/t": "yfc_full_mill_cost",
                 "MTP fix Euro/t": "yfc_mill_fix_cost",
                 "MTP Variable Costs EUR/T calc.": "yfc_mill_var_cost",
                 "Full Cost Euro/t": "yfc_full_cost",
                 "Metal with yield&scrap Euro/t": "yfc_raw_material_costs",
                 "Freight provision Euro/t": "yfc_freight_provision",
                 'Order': "yfc_order_number",
                 'Item': "yfc_item_number",
                 'Euro/t': 'yfc_net_price',
                 "Bruttopreis EUR/T(AE)": "yfc_gross_price",
                 'tons booked': 'yfc_tons_booked_per_item'
                 })

    #Applied filters
    df_yellowfin_costs = cbr.filter_high_mtp_var_costs(df_yellowfin_costs, logger)
    df_yellowfin_costs = cbr.filter_low_high_brut_prices(df_yellowfin_costs, logger)
    df_yellowfin_costs = cbr.filter_non_numerical_order_number(df_yellowfin_costs, 'yfc_order_number', logger)

    df_yellowfin_costs['yfc_order_number'] = df_yellowfin_costs['yfc_order_number'].astype(int)
    df_yellowfin_costs['yfc_item_number'] = df_yellowfin_costs['yfc_item_number'].astype(int)

    df_yellowfin_costs = cbr.create_column_order_item(df_yellowfin_costs, 'yfc_order_item',
                                                      'yfc_order_number', 'yfc_item_number')

    df_yellowfin_costs = cbr.calc_add_column(df_yellowfin_costs, cbr.fix_zero_net_price, logger)

    # remove duplicates order_item due to yfc_tons_booked column
    #list_colum_groupby = [col for col in df_yellowfin_costs.columns if col not in ['yfc_tons_booked_per_item']]
    #df_yellowfin_costs = df_yellowfin_costs.groupby(list_colum_groupby).sum().reset_index()
    df_yellowfin_costs = df_yellowfin_costs.drop_duplicates(subset=['yfc_order_item', 'yfc_tons_booked_per_item'])

    # fix duplicates on other rows
    list_colum_groupby = [col for col in df_yellowfin_costs.columns if col not in \
                          ['yfc_net_price', 'yfc_tons_booked_per_item', 'yfc_mill_fix_cost'
                              , 'yfc_full_mill_cost', 'yfc_mill_var_cost', 'yfc_freight_provision']]
    df_yellowfin_costs = df_yellowfin_costs.groupby(list_colum_groupby) \
        .agg({'yfc_net_price': 'max',
              'yfc_tons_booked_per_item': 'sum',
              'yfc_mill_fix_cost': 'max',
              'yfc_full_mill_cost': 'max',
              'yfc_mill_var_cost': 'max',
              'yfc_freight_provision': 'max'}).reset_index()

    df_yellowfin_costs = cbr.filter_low_tons_booked_per_item(df_yellowfin_costs)

    df_yellowfin_costs = cbr.calc_add_column(df_yellowfin_costs, cbr.add_column_calc_mvc, logger, 'calc_mvc')
    df_yellowfin_costs = cbr.calc_add_column(df_yellowfin_costs, cbr.add_column_calc_mvc_tons, logger, 'calc_total_mvc')
    df_yellowfin_costs = cbr.calc_add_column(df_yellowfin_costs, cbr.add_column_calc_total_revenue, logger,
                                             'calc_total_revenue')

    logger.info("df_yellowfin_costs length (end read_yellowfin_costs) {}".format(len(df_yellowfin_costs)))

    return df_yellowfin_costs


def read_ict_round(filename, list_country_eu_geo, logger):
    logger.info("Read ICT round file: " + filename)
    df_ict_ext_round = utils.strip_df_col_values(pd.read_csv(filename)).drop(columns='highest_grade')
    df_ict_ext_round = cbr.filter_mills(df_ict_ext_round, 'Mill_Name')
    df_ict_ext_round = df_ict_ext_round[['Length', 'grade_classes_of_products', 'Company_Group', 'Mill_Name', 'Country'
        , 'Manufacturing_process_list', 'Qualification_list', 'Theoretical_max_length_in_m'
        , 'source_date', 'OD(mm)', 'AWT(mm)']] \
        .rename(columns={'Length': 'ict_length'
        , 'grade_classes_of_products': 'ict_grade_classes_of_products'
        , 'Company_Group': 'ict_manufacturer_group'
        , 'Mill_Name': 'ict_mill_manufacturer_name'
        , 'Country': 'ict_country'
        , 'Manufacturing_process_list': 'ict_manufacturing_process_list'
        , 'Qualification_list': 'ict_qualification_list'
        , 'Theoretical_max_length_in_m': 'ict_theoretical_max_length_in_m'
        , 'source_date': 'ict_source_date'
        , 'OD(mm)': 'ict_od_mm'
        , 'AWT(mm)': 'ict_awt_mm'})

    logger.info("Round countries :")

    df_ict_ext_round = cbr.fix_country_names(df_ict_ext_round, 'ict_country')

    logger.info(df_ict_ext_round['ict_country'].unique())
    df_ict_ext_round = cbr.filter_eu_countries(df_ict_ext_round, colname='ict_country', dropna=False)
    logger.info(df_ict_ext_round['ict_country'].unique())

    df_ict_ext_round = df_ict_ext_round.drop(columns='ict_country')

    #remove values we cannot convert to numeric
    df_ict_ext_round['ict_length'] = df_ict_ext_round['ict_length'].apply \
        (lambda value: None if (str(value).lower() == 'x') | (value == '----') | (str(value).find("-") > -1) else value)

    df_ict_ext_round['ict_length'] = df_ict_ext_round['ict_length'].apply(lambda x: utils.convert_column_to_float(x))

    return df_ict_ext_round


def read_ict_square(filename, list_country_eu_geo, logger):
    logger.info("Read ICT square file: " + filename)

    df_ict_ext_square = utils.strip_df_col_values(pd.read_csv(filename))
    df_ict_ext_square = cbr.filter_mills(df_ict_ext_square, 'Mill_Name')
    df_ict_ext_square = df_ict_ext_square[['Length', 'grade_classes_of_products', 'Company_Group', 'Mill_Name', 'Country'
        , 'Manufacturing_process_list', 'Qualification_list', 'Theoretical_max_length_in_m'
        , 'source_date', 'AWT(mm)', 'Height', 'Width']] \
        .rename(columns={'Length': 'ict_length'
        , 'grade_classes_of_products': 'ict_grade_classes_of_products'
        , 'Company_Group': 'ict_manufacturer_group'
        , 'Mill_Name': 'ict_mill_manufacturer_name'
        , 'Country': 'ict_country'
        , 'Manufacturing_process_list': 'ict_manufacturing_process_list'
        , 'Qualification_list': 'ict_qualification_list'
        , 'Theoretical_max_length_in_m': 'ict_theoretical_max_length_in_m'
        , 'source_date': 'ict_source_date'
        , 'AWT(mm)': 'ict_awt_mm'
        , 'Height': 'ict_height'
        , 'Width': 'ict_width'})

    logger.info("Square countries :")

    df_ict_ext_square = cbr.fix_country_names(df_ict_ext_square, 'ict_country')

    logger.info(df_ict_ext_square['ict_country'].unique())
    df_ict_ext_square = cbr.filter_eu_countries(df_ict_ext_square, colname='ict_country', dropna=False)
    logger.info(df_ict_ext_square['ict_country'].unique())

    df_ict_ext_square = df_ict_ext_square.drop(columns='ict_country')

    return df_ict_ext_square


def read_map_enduser_file(filename, logger):
    logger.info("Read enduser mapping file: " + filename)
    df_map_enduser = utils.strip_df_col_values(pd.read_csv(filename, encoding='latin-1', sep=';').dropna())
    df_map_enduser = df_map_enduser.rename(columns={'Enduser Text 1 (B)': 'yfb_enduser_cpy_group_tmp',
                                                  'Enduser Group (refers to Enduser Text)': 'map_enduser_cpy_group'})

    df_map_enduser = df_map_enduser.drop_duplicates(subset='yfb_enduser_cpy_group_tmp')

    return df_map_enduser


def read_map_market_segment_file(filename, logger):
    logger.info("Read market segment mapping file: " + filename)
    df_map_market_segment = pd.read_csv(filename, encoding='latin-1', sep=';')\
                                .dropna()\
                                .rename(columns={'Enduser Text 1 (B)': 'yfb_enduser_cpy_group_tmp',
                                                'Marketing Segment (refers to Enduser Text)': 'map_marketing_segment'})
    return df_map_market_segment


def read_map_orderer_file(filename, logger):
    logger.info("Read orderer mapping file: " + filename)
    df_map_order_file = utils.strip_df_col_values(
        pd.read_csv(filename, encoding='latin-1', sep=';').dropna()) \
        .rename(columns={'Orderer': 'yfb_orderer_name',
                         'Orderer Group (refers to Orderer)': 'map_orderer_cpy_group',
                         'Orderer Type (refers to Orderer)': 'map_orderer_type'})
    return df_map_order_file


def read_map_grade_file(filename, logger):
    logger.info("Read grade mapping file: " + filename)
    df_mapping_mill_grade = utils.strip_df_col_values(pd.read_excel(filename, "Map")).drop(
        columns=['VNTU Grade class'])

    #Sanitize xls data
    df_mapping_mill_grade = utils.sanitize_str_df(df_mapping_mill_grade,
                                                  df_mapping_mill_grade.select_dtypes(include='object').columns,
                                                  logger)

    df_mapping_mill_grade = df_mapping_mill_grade.drop_duplicates(subset=['Grade', 'grade class']) \
        .drop_duplicates(subset=['Grade'])[['Grade', 'grade class']] \
        .rename(columns={'grade class': 'class'})

    df_mapping_mill_grade = df_mapping_mill_grade.add_prefix('map_grade_')
    df_mapping_mill_grade = utils.to_lowercase(df_mapping_mill_grade, 'map_grade_Grade')
    df_mapping_mill_grade = utils.to_lowercase(df_mapping_mill_grade, 'map_grade_class')

    df_mapping_mill_grade = df_mapping_mill_grade\
        .drop_duplicates(subset=['map_grade_Grade', 'map_grade_class']) \
        .drop_duplicates(subset=['map_grade_Grade'])

    return df_mapping_mill_grade


def read_map_mill_name(filename, logger):
    logger.info("Read mill mapping file: " + filename)
    df_mapping_mills_name = utils.strip_df_col_values(pd.read_csv(filename)) \
        .rename(columns={'ICT Mill Names': 'map_mill_ict_names_tmp',
                         'Yellow_mill_name': 'map_mill'})
    df_mapping_mills_name = cbr.filter_mills(df_mapping_mills_name, 'map_mill')

    return df_mapping_mills_name


def read_ref_file(filename, logger):
    logger.info("Read ref Vallourec ref file: " + filename)
    df_ref = utils.strip_df_col_values(pd.read_csv(filename))
    df_ref = df_ref.add_prefix('ref_val_')
    return df_ref


def read_vntu_price_list_file(filename, logger):
    logger.info("Read VNTU price list file: " + filename)
    df_vntu_price_list = pd.read_csv(filename).drop(columns='Unnamed: 0') \
        [['Country', 'Grade', 'sheet_name', 'OD min', 'OD max', 'WT min', 'WT max', 'DDP, EUR/t', 'Freigth Eur/t', 'VC'
            , 'TP', 'Margin TP based', 'Margin VLR, LTSA based', 'Δ to previous month', 'Δ to market price indication'
            , 'items from rerolled billets  from Assel marked gray in Size range', 'surcharge for fixed length',
          'WT > 60 mm'
            , 'Surcharge for TPI in addition to inspection cost']] \
        .rename(columns={
        'Country': 'vntu_country'
        , 'Grade': 'vntu_grade'
        , 'sheet_name': 'vntu_sheet_name'
        , 'OD min': 'vntu_od_min'
        , 'OD max': 'vntu_od_max'
        , 'WT min': 'vntu_wt_min'
        , 'WT max': 'vntu_wt_max'
        , 'DDP, EUR/t': 'vntu_gross_price'
        , 'Freigth Eur/t': 'vntu_freight'
        , 'VC': 'vntu_vc'
        , 'TP': 'vntu_tp'
        , 'Margin TP based': 'vntu_margin_tp_based'
        , 'Margin VLR, LTSA based': 'vntu_margin_vlr_ltsa_based'
        , 'Δ to previous month': 'vntu_delta_to_previous_month'
        , 'Δ to market price indication': 'vntu_delta_to_market_price_indication'
        ,
        'items from rerolled billets  from Assel marked gray in Size range': 'vntu_items_from_rerolled_billets_ from_assel_marked_gray_in_size_range'
        , 'surcharge for fixed length': 'vntu_surcharge_for_fixed_length'
        , 'WT > 60 mm': 'vntu_surcharge_wt_sup_60'
        , 'Surcharge for TPI in addition to inspection cost': 'vntu_surcharge_add_tpi'
    })
    return df_vntu_price_list
    
def merge_yellowfin_map(grouping_tech_var_file, df_yellowfin_bookings, df_yellowfin_costs, df_map_order_file \
                        , df_map_market_segment, df_map_enduser, df_mapping_mill_grade, df_mapping_mills_name, logger):

    logger.info("Mapping Yellowfin files and Yellowfin with other mapping files")
    """
    # Creating new column, if Grade_Segmentation_B = NA or 'Others', return grade
    def get_mixed_grade(row, grade_seg, intern_grade):
        if (row[grade_seg] == row[grade_seg]) \
                & (row[grade_seg] != None) \
                & (row[grade_seg] != np.nan) \
                & (row[grade_seg] != 'nan') \
                & (row[grade_seg] != 'Others') \
                & (row[grade_seg] != 'Low_Engi_MW') \
                & (row[grade_seg] != 'Premium') \
                & (row[grade_seg] != 'Carbon') \
                & (row[grade_seg] != 'Offshore series'):
            return row[grade_seg].lower()
        else:
            return row[intern_grade].lower()

    df_yellowfin_bookings['yfb_mixed_grade'] = df_yellowfin_bookings.apply(
        lambda row: get_mixed_grade(row, 'yfb_grade_segmentation_tmp', 'yfb_internal_grade_name'), axis=1)"""

    #TODO : replace nested merge with sequential code easier to understand
    # df_merged_yellowfin = pd.merge(left= \
    #                                    pd.merge(left= \
    #                                                 pd.merge(left= \
    #                                                              # pd.merge(left= \
    #                                                          pd.merge(left=df_yellowfin_bookings,
    #                                                                   right=df_yellowfin_costs
    #                                                                   , how='inner', left_on='yfb_order_item',
    #                                                                   right_on='yfc_order_item') \
    #                                                          # , right=df_yellowfin_invoices \
    #                                                          # , how='left', left_on = 'yfb_order_item', right_on = 'yfi_order_item')
    #                                                          , right=df_map_order_file.drop_duplicates(
    #                                                         subset='yfb_orderer_name'), how='left'
    #                                                          , on='yfb_orderer_name')
    #                                             , right=df_map_market_segment, how='left'
    #                                             , on='yfb_enduser_cpy_group_tmp')
    #                                , right=df_map_enduser, how='left'
    #                                , on='yfb_enduser_cpy_group_tmp')

    df_bookings_costs = pd.merge(left=df_yellowfin_bookings,
                                 right=df_yellowfin_costs,
                                 how='inner',
                                 left_on='yfb_order_item',
                                 right_on='yfc_order_item')
    df_bookings_costs_orders = pd.merge(left=df_bookings_costs,
                                        right=df_map_order_file.drop_duplicates(subset='yfb_orderer_name'),
                                        how='left',
                                        on='yfb_orderer_name')
    df_bookings_costs_orders_markets = pd.merge(left=df_bookings_costs_orders,
                                                right=df_map_market_segment,
                                                how='left',
                                                on='yfb_enduser_cpy_group_tmp')
    df_merged_yellowfin = pd.merge(left=df_bookings_costs_orders_markets,
                                   right=df_map_enduser,
                                   how='left',
                                   on='yfb_enduser_cpy_group_tmp')
    #function call
    dico = utils.read_grouping_tech_var_file(grouping_tech_var_file, logger)
    
    for col_name, col in dico.items():
        df_merged_yellowfin[col_name] = df_merged_yellowfin[col_name].str.upper().replace(col)
    
    #DONE added no value as a possible missing placeholder
    #TODO : This statement has to be a shared utility function 
    df_merged_yellowfin = utils.replace_missing_values(df_merged_yellowfin)

    logger.info("Merge Yellowfin with mapping tables")
    # merge df_yellowfin_bookings with mapping tables
    df_merged_yellowfin = \
        pd.merge(left= \
                     pd.merge(left=df_merged_yellowfin, \
                              right=df_mapping_mill_grade \
                              , how='left' \
                              , left_on='yfb_internal_grade_name' \
                              , right_on='map_grade_Grade') \
                 , right=df_mapping_mills_name, how='left', left_on='yfb_delivery_plant_tmp', right_on='map_mill')

    logger.info("df_merged_yellowfin length (end merge_yellowfin_map) {}".format(len(df_merged_yellowfin)))

    return df_merged_yellowfin

# def read_grouping_tech_var_file(grouping_tech_var_file, logger):
#     from openpyxl import load_workbook
#
#     logger.info("Reading file {}".format(grouping_tech_var_file))
#
#     xl = pd.ExcelFile(grouping_tech_var_file, engine='openpyxl')
#     header_ind = 2
#     df_0 = xl.parse(sheet_name=0, header=header_ind)
#
#     wb = load_workbook(filename=grouping_tech_var_file, read_only=False)
#     sheets = wb.sheetnames
#     sheet_0 = wb[sheets[0]]
#
#     #DONE fixed merged cells bug
#     for e in sheet_0.merged_cells:
#         c1, r1, c2, r2 = e.bounds
#         top_value = sheet_0.cell(r1,c1).value
#         df_0.iloc[r1 - header_ind - 1:r2 - header_ind - 1, c1-1] = top_value
#
#     logger.info(df_0['Unnamed: 5'].dropna())
#
#     df_1 = xl.parse(sheet_name=1, header=header_ind)
#     sheet_1 = wb[sheets[1]]
#
#     for e in sheet_1.merged_cells:
#         c1, r1, c2, r2 = e.bounds
#         top_value = sheet_1.cell(r1,c1).value
#         df_1.iloc[r1 - header_ind - 1:r2 - header_ind - 1, c1-1] = top_value
#
#     #DONE rename all the df_0 and df_1 columns
#     df_0 = df_0.rename(columns={'Length type': 'yfb_length_type',
#                                 'Ultrasound Name': 'yfb_ultrasound_type',
#                                 'Heat Treatment Name': 'yfb_heat_treatment',
#                                 'Ext. Corner Prof. Name (B)': 'yfb_ext_corner_radius',
#                                 'Curvature Name (B)': 'yfb_curvature_tolerance'}).dropna(how='all', axis=1).dropna(how='all').reset_index(drop=True)
#
#     logger.info("df_0 columns are {}".format(df_0.columns))
#
#     df_1 = df_1.rename(columns={'Leading Norm 3 Name (B)': "yfb_norm_3"
#         , 'Leading Norm 2 Name (B)': 'yfb_norm_2'
#         , "Leading Norm 1 Name (B)": "yfb_norm_1"
#         , "Grade 1 Norm Name (B)": "yfb_grade_1"
#         , "Grade 2 Norm Name (B)": "yfb_grade_2"
#         , "Grade 3 Norm Name (B)": "yfb_grade_3"}).dropna(how='all', axis=1).dropna(how='all').reset_index(drop=True)
#
#     logger.info("df_1 columns are {}".format(df_1.columns))
#
#     #DONE Concat keeping all the df_0 and df_1 columns
#     df_dico = pd.concat([df_0, df_1], axis=1)[['yfb_length_type', 'Group with',
#     'yfb_heat_treatment', 'Group with.1', 'yfb_ultrasound_type', 'Group with.2',
#     'yfb_ext_corner_radius', 'Group with.3', 'yfb_curvature_tolerance',
#     'Group with ', 'yfb_norm_3', 'Grouping', 'yfb_norm_2', 'Grouping.1',
#     'yfb_norm_1', 'Grouping.2', 'yfb_grade_3', 'Grouping.3', 'yfb_grade_2',
#     'Grouping.4', 'yfb_grade_1', 'Grouping.5']]
#
#     logger.info("df_dico columns are {}".format(df_dico.columns))
#     logger.info("df_dico columns number {}".format(len(df_dico.columns)))
#
#     dico = {}
#     for i in range(int(len(df_dico.columns) / 2)):
#         dico_col = {}
#         for ind, val in df_dico.iloc[:, 2 * i + 1].dropna().items():
#             dico_col[df_dico.iloc[ind, 2 * i].strip().upper()] = val.strip().upper()
#
#         dico[df_dico.columns[int(2 * i)]] = dico_col
#
#     return dico

def get_yellowfin_ref_od_wt(df_merged_yellowfin, df_od_wt_ref, logger):
    logger.info('Get yellowfin OD WT vallourec reference values')
    df_merged_yellowfin["map_ref_val_diameter_height"] = df_merged_yellowfin['yfb_diameter_height']
    df_merged_yellowfin["map_ref_val_wall_thickness"] = df_merged_yellowfin['yfb_wall_thickness']
    for od_mm in df_merged_yellowfin['yfb_diameter_height'].unique():
        for wt_mm in df_merged_yellowfin[(df_merged_yellowfin['yfb_diameter_height'] == od_mm)]\
                                                                ['yfb_wall_thickness'].unique():

            if len(df_od_wt_ref[(df_od_wt_ref['ref_val_od_mm'] == od_mm) & \
                                (df_od_wt_ref['ref_val_wt_mm'] == wt_mm)]) == 0:

                list_od_ref = list(df_od_wt_ref['ref_val_od_mm'].unique())
                list_wt_ref = list(df_od_wt_ref['ref_val_wt_mm'].unique())

                closest_ref_number_wt = min(list_wt_ref, key=lambda x: abs(x - wt_mm))
                closest_ref_number_od = min(list_od_ref, key=lambda x: abs(x - od_mm))

                od_final = od_mm
                wt_final = wt_mm
                if abs(closest_ref_number_od - od_mm) >= abs(closest_ref_number_wt - wt_mm):
                    wt_final = closest_ref_number_wt
                    if len(df_od_wt_ref[(df_od_wt_ref['ref_val_od_mm'] == od_mm) & \
                                        (df_od_wt_ref['ref_val_wt_mm'] == closest_ref_number_wt)]) == 0:
                        od_final = closest_ref_number_od

                elif abs(closest_ref_number_od - od_mm) < abs(closest_ref_number_wt - wt_mm):
                    od_final = closest_ref_number_od
                    if len(df_od_wt_ref[(df_od_wt_ref['ref_val_od_mm'] == closest_ref_number_od) & \
                                        (df_od_wt_ref['ref_val_wt_mm'] == wt_mm)]) == 0:
                        wt_final = closest_ref_number_wt

                df_merged_yellowfin.loc[(df_merged_yellowfin['yfb_diameter_height'] == od_mm) & \
                                        (df_merged_yellowfin['yfb_wall_thickness'] == wt_mm) \
                    , 'map_ref_val_diameter_height'] = od_final

                df_merged_yellowfin.loc[(df_merged_yellowfin['yfb_diameter_height'] == od_mm) & \
                                        (df_merged_yellowfin['yfb_wall_thickness'] == wt_mm) \
                    , 'map_ref_val_wall_thickness'] = wt_final

    return df_merged_yellowfin


def is_matching_row_prio1(row, value_od, value_wt):
    if (row['vntu_wt_min'] == row['vntu_wt_min']) & (row['vntu_wt_max'] == row['vntu_wt_max']):
        if (value_wt == row['vntu_wt_min']) & (row['vntu_od_min'] <= value_od <= row['vntu_od_max']):
            return True
    elif row['vntu_od_min'] == value_od:
        return True


def is_matching_row_prio2(row, value_od, value_wt):
    if (row['vntu_wt_min'] == row['vntu_wt_min']) & (row['vntu_wt_max'] != row['vntu_wt_max']):
        if (value_wt > row['vntu_wt_min']) & (row['vntu_od_min'] <= value_od <= row['vntu_od_max']):
            return True
    elif (row['vntu_wt_min'] != row['vntu_wt_min']) & (row['vntu_wt_max'] == row['vntu_wt_max']):
        if (value_wt < row['vntu_wt_max']) & (row['vntu_od_min'] <= value_od <= row['vntu_od_max']):
            return True


def is_matching_row_prio3(row, value_od):
    if row['vntu_od_min'] <= value_od <= row['vntu_od_max']:
        return True


def clean_od_category(x):
    try:
        return int(x)
    except:
        if (x == x) & (x != ''):
            return int(x.replace('[', '').replace(']', '').replace(' ', '').split(',')[0])


def clean_vntu_country(x):
    list_spain = ['Portugal+distant Spain', 'Spain (central Spain)'
        , 'Spain (Barcelona)', 'Spain (Madrid, Barcelona, Bilbao)']
    if x in list_spain:
        return 'Spain'
    elif x == 'England':
        return 'Great Britain'
    else:
        return x


def merge_vntu(df_merged_yellowfin, df_vntu_price_list, logger):
    logger.info("merge VNTU with Yellowfin")

    df_merged_yellowfin['od_categ'] = ""
    df_vntu = df_merged_yellowfin[((df_merged_yellowfin['map_grade_class'] == 'commodity') \
                                   & (df_merged_yellowfin["calc_pipe_type"] == "round"))]
    df_not_vntu = df_merged_yellowfin[((df_merged_yellowfin['map_grade_class'] != 'commodity') \
                                       | (df_merged_yellowfin["calc_pipe_type"] != "round"))]

    df_vntu_price_list['vntu_grade'] = df_vntu_price_list['vntu_grade'].str.replace(u'\xa0', u'')
    # adding exception on 298.5
    df_vntu_price_list_tmp = df_vntu_price_list[df_vntu_price_list['vntu_od_min'] != 298.5]
    df_vntu_price_list_except1 = df_vntu_price_list[df_vntu_price_list['vntu_od_min'] == 298.5]
    df_vntu_price_list_except2 = df_vntu_price_list[df_vntu_price_list['vntu_od_min'] == 298.5]
    df_vntu_price_list_except1['vntu_od_max'] = 298.5
    df_vntu_price_list_except2['vntu_od_min'] = 323.9
    df_vntu_price_list_except1['vntu_wt_min'] = np.nan
    df_vntu_price_list_except2['vntu_wt_min'] = np.nan
    df_vntu_price_list_except1['vntu_wt_max'] = np.nan
    df_vntu_price_list_except2['vntu_wt_max'] = np.nan
    df_vntu_price_list = pd.concat([df_vntu_price_list_tmp, df_vntu_price_list_except1, df_vntu_price_list_except2])

    #DONE fixed the grade pattern from 'S355J2H, E355+N, P355NH,St52' to 'S355J2H, E355+N, P355NH, St52'
    list_od_vntu = df_vntu_price_list[df_vntu_price_list['vntu_grade'] == 'S355J2H, E355+N, P355NH, St52'] \
        [['vntu_od_min', 'vntu_od_max', 'vntu_wt_min', 'vntu_wt_max']].drop_duplicates() \
        .reset_index(drop=True).reset_index() \
        .rename(columns={'index': 'od_categ'})

    df_vntu_priority1 = pd.concat([list_od_vntu.dropna(), list_od_vntu[
        (list_od_vntu['vntu_od_min'] == list_od_vntu['vntu_od_max'])]]).drop_duplicates()

    df_vntu_priority2 = list_od_vntu[(((list_od_vntu['vntu_wt_min'] != list_od_vntu['vntu_wt_min']) \
                                       & (list_od_vntu['vntu_wt_max'] == list_od_vntu['vntu_wt_max'])) \
                                      | ((list_od_vntu['vntu_wt_min'] == list_od_vntu['vntu_wt_min']) \
                                         & (list_od_vntu['vntu_wt_max'] != list_od_vntu['vntu_wt_max']))) \
                                     & (list_od_vntu['vntu_od_min'] != list_od_vntu['vntu_od_max'])]

    df_vntu_priority3 = list_od_vntu[((list_od_vntu['vntu_wt_min'] != list_od_vntu['vntu_wt_min']) \
                                      & (list_od_vntu['vntu_wt_max'] != list_od_vntu['vntu_wt_max']) \
                                      & (list_od_vntu['vntu_od_min'] != list_od_vntu['vntu_od_max']))]

    df_vntu_column_od = 'yfb_diameter_height'
    df_vntu_column_wt = 'yfb_wall_thickness'

    for od_mm in df_vntu[df_vntu_column_od].sort_values().unique():
        for wt_mm in df_vntu[df_vntu[df_vntu_column_od] == od_mm][df_vntu_column_wt].sort_values().unique():

            df_vntu_priority1['result'] = df_vntu_priority1 \
                                            .apply(lambda row: is_matching_row_prio1(row, od_mm, wt_mm), axis=1)

            if len(df_vntu_priority1[df_vntu_priority1['result'] == True]) == 1:
                df_vntu.loc[(df_vntu[df_vntu_column_od] == od_mm) & \
                            (df_vntu[df_vntu_column_wt] == wt_mm) \
                    , 'od_categ'] = df_vntu_priority1[df_vntu_priority1['result'] == True]['od_categ'].iloc[0]
                continue

            elif len(df_vntu_priority1[df_vntu_priority1['result'] == True]) > 1:
                df_vntu.loc[(df_vntu[df_vntu_column_od] == od_mm) & \
                            (df_vntu[df_vntu_column_wt] == wt_mm) \
                    , 'od_categ'] = str(list(df_vntu_priority1[df_vntu_priority1['result'] == True]['od_categ']))
                continue

            df_vntu_priority2['result'] = df_vntu_priority2.apply(lambda row: is_matching_row_prio2(row, od_mm, wt_mm),
                                                                  axis=1)

            if len(df_vntu_priority2[df_vntu_priority2['result'] == True]) == 1:
                df_vntu.loc[(df_vntu[df_vntu_column_od] == od_mm) & \
                            (df_vntu[df_vntu_column_wt] == wt_mm) \
                    , 'od_categ'] = df_vntu_priority2[df_vntu_priority2['result'] == True]['od_categ'].iloc[0]
                continue

            elif len(df_vntu_priority2[df_vntu_priority2['result'] == True]) > 1:
                df_vntu.loc[(df_vntu[df_vntu_column_od] == od_mm) & \
                            (df_vntu[df_vntu_column_wt] == wt_mm) \
                    , 'od_categ'] = str(list(df_vntu_priority2[df_vntu_priority2['result'] == True]['od_categ']))
                continue

            df_vntu_priority3['result'] = df_vntu_priority3.apply(lambda row: is_matching_row_prio3(row, od_mm), axis=1)

            if len(df_vntu_priority3[df_vntu_priority3['result'] == True]) == 1:
                df_vntu.loc[(df_vntu[df_vntu_column_od] == od_mm) & \
                            (df_vntu[df_vntu_column_wt] == wt_mm) \
                    , 'od_categ'] = df_vntu_priority3[df_vntu_priority3['result'] == True]['od_categ'].iloc[0]
                continue

            elif len(df_vntu_priority3[df_vntu_priority3['result'] == True]) > 1:
                df_vntu.loc[(df_vntu[df_vntu_column_od] == od_mm) & \
                            (df_vntu[df_vntu_column_wt] == wt_mm) \
                    , 'od_categ'] = str(list(df_vntu_priority3[df_vntu_priority3['result'] == True]['od_categ']))
                continue
            else:
                df_vntu.loc[(df_vntu[df_vntu_column_od] == od_mm) & \
                            (df_vntu[df_vntu_column_wt] == wt_mm) \
                    , 'od_categ'] = np.nan

    df_vntu['od_categ'] = df_vntu['od_categ'].apply(lambda x: clean_od_category(x))

    df_not_vntu['od_categ'] = np.nan
    df_merge_vntu = pd.concat([df_vntu, df_not_vntu])

    df_vntu_price_list = df_vntu_price_list[df_vntu_price_list['vntu_grade'] == 'S355J2H, E355+N, P355NH,St52']
    df_vntu_price_list = pd.merge(left=df_vntu_price_list, right=list_od_vntu
                                  , how='left', on=['vntu_od_min', 'vntu_od_max', 'vntu_wt_min', 'vntu_wt_max'])

    df_vntu_price_list = df_vntu_price_list[df_vntu_price_list['od_categ'] == df_vntu_price_list['od_categ']]
    df_vntu_price_list = df_vntu_price_list[~df_vntu_price_list['od_categ'].astype(str).str.contains('\[')]

    df_vntu_price_list['vntu_country'] = df_vntu_price_list['vntu_country'].apply(lambda x: clean_vntu_country(x))

    df_vntu_price_list['vntu_date'] = df_vntu_price_list['vntu_sheet_name'] \
        .apply(lambda x: datetime.strptime(str("{:.2f}".format(x)), "%Y.%m").strftime("%d%m%Y"))

    df_merge_vntu['yfb_date_first_intake_hrmonized_joined_vntu'] = df_merge_vntu['yfb_date_creation'] \
        .apply(lambda x: datetime.strptime(str(x), "%d/%m/%Y").replace(day=1).strftime("%d%m%Y"))
    df_vntu_price_list = df_vntu_price_list[
        ~df_vntu_price_list.duplicated(subset=['vntu_country', 'od_categ', 'vntu_date'], keep='last')]

    df_yellowfin_bookings_map = pd.merge(left=df_merge_vntu, right=df_vntu_price_list, how='left'
                                         , left_on=['yfb_enduser_country', 'od_categ',
                                                    'yfb_date_first_intake_hrmonized_joined_vntu']
                                         , right_on=['vntu_country', 'od_categ', 'vntu_date'])
    return df_yellowfin_bookings_map

def split_ict_commodity(df_ict, logger):
    logger.info("Split commodity grade class")
    df_map = pd.DataFrame()
    df_map['ict_grade_classes_of_products'] = ['commodity', 'commodity', 'commodity']
    df_map['grade_dest'] = ['standard steel constr./mech. eng.', 'simple boiler', 'simple steel constr.']

    df_ict_commodity = df_ict[df_ict['ict_grade_classes_of_products'] == 'commodity']
    df_ict_not_commodity = df_ict[df_ict['ict_grade_classes_of_products'] != 'commodity']

    df_ict_commodity = pd.merge(left=df_ict_commodity, right=df_map, how='left', on='ict_grade_classes_of_products')\
        .drop(columns='ict_grade_classes_of_products')\
        .rename(columns={'grade_dest': 'ict_grade_classes_of_products'})

    df_ict = pd.concat([df_ict_not_commodity, df_ict_commodity], sort=False)

    return pd.concat([df_ict_not_commodity, df_ict_commodity], sort=False)


def treat_ict(df_ict, df_val_ref, pipe_type, logger):
    logger.info("Treat ICT " + str(pipe_type))
    logger.info("df_ict length (before) {}".format(len(df_ict)))
    # TODO add to mapping file instead of here
    df_ict = cbr.rename_grade_value(df_ict, 'simple QT grades', 'simple QT')

    df_ict['ict_grade_classes_of_products'] = df_ict['ict_grade_classes_of_products'] \
        .str.strip() # .str.split('(').str[0].str.strip()

    if pipe_type == 'round':
        df_ict = utils.get_closest_tubes_from_val_ref(df_ict, df_val_ref)
    elif pipe_type == 'square':
        df_ict = utils.get_closest_height_width_tube_from_vall_ref(df_ict, df_val_ref, 'ict_height', 'ict_width', logger)
        df_ict =  utils.get_closest_square_wt_tube_from_vall_ref(df_ict, df_val_ref, 'ict_awt_mm', logger)
    else:
        raise NameError("Wrong pipe_type: Either 'round' or 'square'")

    df_ict = utils.to_lowercase(df_ict, 'ict_grade_classes_of_products')
    logger.info("df_ict length (after) {}".format(len(df_ict)))
    return df_ict


def merge_ict_yellowfin(df_ict_ext_round, df_ict_ext_square, 
                df_yellowfin_bookings_round, df_yellowfin_bookings_square, df_yellowfin_bookings_others,
                 logger):
    """Merge ICT and yellowfin Data

    Args:
        df_ict_ext_round (DataFrame): ICT round DataFrame
        df_ict_ext_square (DataFrame): ICT square DataFrame
        df_yellowfin_bookings_round (DataFrame): Yellowfin bookings round DataFrame
        df_yellowfin_bookings_square (DataFrame): Yellowfin bookings square DataFrame
        df_yellowfin_bookings_others (DataFrame): Yellowfin bookings others DataFrame
        logger (Logger): [description]

    Returns:
        DataFrame: Merged ICT and Yellowfin DataFrame 
    """
    logger.info("Merge Yellowfin with ICT")

    """
    # Need to drop duplicates since we are not working at the detail of the grade but the frade class  
    df_ict_ext_round = df_ict_ext_round \
        .sort_values('ict_length', ascending=False) \
        .drop_duplicates(['ict_mill_manufacturer_name', 'ict_grade_classes_of_products',
                          'map_ref_val_diameter_height', 'map_ref_val_wall_thickness'])
    
    logger.info("df_ict_ext_round length (after) {}".format(len(df_ict_ext_round)))
    """
    logger.info("df_ict_ext_round columns are {}".format(df_ict_ext_round.columns))
    logger.info("df_ict_ext_round columns types are {}".format(df_ict_ext_round.dtypes))
    logger.info("df_ict_ext_round length (before) {}".format(len(df_ict_ext_round)))
    # Drop potential duplicates in the right joined DataFrame to avoid duplication in output
    df_ict_ext_round = df_ict_ext_round.drop_duplicates(subset=['ict_grade_classes_of_products'
        , 'map_ref_val_diameter_height'
        , 'map_ref_val_wall_thickness'])

    logger.info("df_ict_ext_round length (after) {}".format(len(df_ict_ext_round)))

    logger.info("Size of df_yellowfin_bookings_round : {}".format(len(df_yellowfin_bookings_round)))

    # merge YellowFin with ICT round
    df_merge_round = pd.merge(left=df_yellowfin_bookings_round, right=df_ict_ext_round, how='left' \
                              , left_on=['map_grade_class', 'map_ref_val_diameter_height', 'map_ref_val_wall_thickness'] \
                              , right_on=['ict_grade_classes_of_products', 'map_ref_val_diameter_height',
                                          'map_ref_val_wall_thickness'])
    logger.info("df_merge_round length {}".format(len(df_merge_round)))

    logger.info("Merge YellowFin with ICT square")
    # merge YellowFin with ICT square

    df_ict_ext_square = df_ict_ext_square.drop_duplicates(subset=['ict_grade_classes_of_products',
                                                        'map_ref_val_diameter_height', 'map_ref_val_width'
                                                        , 'map_ref_val_wall_thickness'])

    logger.info("df_yellowfin_bookings_square length (before) {}".format(len(df_yellowfin_bookings_square)))
    df_merge_square = pd.merge(left=df_yellowfin_bookings_square, right=df_ict_ext_square, how='left' \
                               , left_on=['map_grade_class', 'map_ref_val_diameter_height', 'map_ref_val_width',
                                          'map_ref_val_wall_thickness'] \
                               , right_on=['ict_grade_classes_of_products', 'map_ref_val_diameter_height',
                                           'map_ref_val_width', 'map_ref_val_wall_thickness'])


    logger.info("df_yellowfin_bookings_square length (after) {}".format(len(df_yellowfin_bookings_square)))

    df_yellowfin_ict_merged = pd.concat([df_merge_square, df_merge_round, df_yellowfin_bookings_others], sort=False)

    logger.info("df_yellowfin_ict_merged length {}".format(len(df_yellowfin_ict_merged)))

    return df_yellowfin_ict_merged


def calc_diff_od_wt(df_yellowfin_bookings_round, logger):
    """Calc diff on OD and WT between Yellowfin and matched OD and WT from vallourec ref

    Args:
        df_yellowfin_bookings_round (DataFrame): Yellowfin bookings DataFrame
        logger (Logger): Logger

    Returns:
        DataFrame: DataFrame with calculated columns
    """
    logger.info("Calc diff OD WT")

    df_yellowfin_bookings_round['diff_od_yf_and_matched'] = \
    abs(df_yellowfin_bookings_round['yfb_diameter_height'] - df_yellowfin_bookings_round[
        'map_ref_val_diameter_height'])

    df_yellowfin_bookings_round['diff_wt_yf_and_matched'] = \
        abs(df_yellowfin_bookings_round['yfb_wall_thickness'] - df_yellowfin_bookings_round[
            'map_ref_val_wall_thickness'])
    return df_yellowfin_bookings_round


def calc_total_number_of_norms(rows):
    total_certificate = 0
    if rows['yfb_norm_1'] != None:
        total_certificate += 1

    if rows['yfb_norm_2'] != None:
        total_certificate += 1

    if rows['yfb_norm_3'] != None:
        total_certificate += 1

    return total_certificate


def calc_straightness_total_deviation_percentage(x):
    if x != None:
        my_re = re.search(pattern=r"\d.\d{1,2}%", string=str(x).replace(' ', ''))
        if my_re:
            return my_re.group()
        else:
            return None
    else:
        return None


def calc_straightness_max_deviation(x):
    if x != None:
        my_re = re.search(pattern=r"(\d(mm/m)|\d.\d(mm/m)|\d.\dmmon\d.\dm)", string=str(x).replace(' ', ''))
        if my_re:
            return my_re.group().replace('on', '/')
        else:
            return None
    else:
        return None


def calc_max_radius(x):
    if x != None:
        my_re = re.search(pattern=r"\dT|\d,\d(x|X|T)|\d(x|X|T)", string=str(x).replace(' ', ''))
        if my_re:
            return my_re.group().replace('X', 'T').replace('x', 'T')
        else:
            return None
    else:
        return None


def calc_columns(df_final, logger):
    logger.info("calculate columns to calculate")
    # calc_total_tons_booked_per_order
    df_final = pd.merge(left=df_final
                        , right=df_final[['yfb_order_number', 'yfc_tons_booked_per_item']] \
                        .groupby('yfb_order_number').sum().reset_index() \
                        .rename(columns={'yfc_tons_booked_per_item': 'calc_total_tons_booked_per_order'})
                        , how='left'
                        , on='yfb_order_number'
                        )

    df_final['calc_total_number_of_norms'] = df_final.apply(lambda rows: calc_total_number_of_norms(rows), axis=1)

    df_final['calc_straightness_total_deviation_percentage'] = df_final['yfb_straightness_tolerance'] \
        .apply(lambda x: calc_straightness_total_deviation_percentage(x))


    df_final['calc_straightness_max_deviation'] = df_final['yfb_straightness_tolerance'] \
        .apply(lambda x: calc_straightness_max_deviation(x))

    #Commented out MVC fix code
    #df_final['calc_mvc'] = df_final['yfc_net_price'] - df_final['yfc_mill_var_cost']
    #df_final['calc_mvc_old_tmp'] = df_final['calc_mvc']
    #df_final['calc_flag_mvc_modified'] = df_final.apply(
    #    lambda row: 1 if row['yfc_net_price'] == row['calc_mvc'] \
    #        else 0, axis=1)
    #df_final['calc_mvc'] = df_final.apply(
    #    lambda row: 0 if row['yfc_net_price'] == row['calc_mvc'] \
    #        else row['calc_mvc'], axis=1)

    #VNTU no longer used
    #df_final['vntu_surcharge_for_fixed_length'] = 0.03
    #df_final['vntu_surcharge_add_tpi'] = 5
    #df_final['vntu_surcharge_wt_sup_60'] = 100
    #df_final['calc_vntu_net_price'] = df_final['vntu_gross_price'] - df_final['vntu_freight']

    list_value_fixed_length = ["COMBINED LENGTH", "FIXED LENGTH", "MULTIPLE LENGTH", "MULTISECTIONAL FIX LENGTH"]
    list_value_fixed_tpi = ['Inspection certificate 3.2 according to EN 10204:2004',
                            'Inspection certificate 3.2 according to EN 10204:2004 by TPI']

    def calc_surcharge(row, column_to_surcharge, list_value_fixed_length, list_value_fixed_tpi):
        if row['yfb_length_type'] in list_value_fixed_length:
            try:
                return row[column_to_surcharge] * row['vntu_surcharge_for_fixed_length'] + row[column_to_surcharge]
            except:
                return None
        elif row['yfb_inspection_certificate'] in list_value_fixed_tpi:
            try:
                return row[column_to_surcharge] + row['vntu_surcharge_add_tpi']
            except:
                return None
        elif row['yfb_wall_thickness'] > 60:
            try:
                return row[column_to_surcharge] + row['vntu_surcharge_wt_sup_60']
            except:
                return None
        else:
            return row[column_to_surcharge]

    # VNTU no longer used
    #df_final['calc_vntu_total_net_price'] = df_final \
    #    .apply(lambda row: calc_surcharge(row, 'calc_vntu_net_price', list_value_fixed_length, list_value_fixed_tpi),
    #           axis=1)

    #df_final['calc_vntu_total_gross_price'] = df_final \
    #    .apply(lambda row: calc_surcharge(row, 'vntu_gross_price', list_value_fixed_length, list_value_fixed_tpi),
    #           axis=1)

    #df_final['vntu_surcharge_for_fixed_length'] = df_final \
    #    .apply(
    #    lambda row: row.vntu_surcharge_for_fixed_length if row.yfb_length_type in list_value_fixed_length else None,
    #    axis=1)

    #df_final['vntu_surcharge_wt_sup_60'] = df_final \
    #    .apply(lambda row: row.vntu_surcharge_wt_sup_60 if float(row.yfb_wall_thickness) > 60 else None, axis=1)

    #df_final['vntu_surcharge_add_tpi'] = df_final \
    #    .apply(
    #    lambda row: row.vntu_surcharge_add_tpi if row.yfb_inspection_certificate in list_value_fixed_tpi else None,
    #    axis=1)

    df_final['calc_month_of_creation'] = df_final['yfb_date_creation'].apply(
        lambda x: datetime.strptime(x, '%d/%m/%Y').month)

    df_final['calc_quarter_of_creation'] = df_final['yfb_date_creation'].apply(
        lambda x: pd.Timestamp(datetime.strptime(x, '%d/%m/%Y')).quarter)

    df_final['calc_max_radius'] = df_final['yfb_ext_corner_radius'].apply(lambda x: calc_max_radius(x))

    return df_final


def order_columns(df, logger):
    """

    :param df:
    :param logger:
    :return:
    """
    logger.info("Order columns")
    df = df[['yfb_order_item', 'yfb_order_number', 'yfb_item_number', 'yfc_order_number', 'yfc_item_number',
             'yfb_date_creation',  'yfb_year_creation',  'calc_quarter_of_creation', 'calc_month_of_creation',
             'calc_pipe_type', 'yfb_diameter_height', 'yfb_wall_thickness', 'yfb_width', 'calc_inside_diameter', 'yfb_enduser_country',
             'yfb_enduser_cpy_group_tmp', 'yfb_enduser_name', 'map_enduser_cpy_group', 'map_grade_class'
             #, 'map_grade_mixed_grade'
             , 'map_marketing_segment', 'map_mill', 'map_mill_ict_names_tmp',
             'yfb_orderer_country', 'yfb_orderer_name', 'map_orderer_cpy_group', 'map_orderer_type',
             'map_ref_val_diameter_height', 'map_ref_val_width', 'map_ref_val_wall_thickness', 'yfc_tons_booked_per_item',
             'calc_total_tons_booked_per_order', 'yfb_net_price', 'calc_mvc', 'calc_total_mvc', 'calc_total_revenue',
             #'calc_mvc_old_tmp', 'calc_flag_mvc_modified',
             'yfb_norm_1', 'yfb_norm_2', 'yfb_norm_3', 'calc_total_number_of_norms',
             'yfb_squareness_tolerance', 'yfb_straightness_tolerance', 'calc_straightness_total_deviation_percentage',
             'calc_straightness_max_deviation', 'calc_max_radius', 'diff_od_yf_and_matched', 'diff_wt_yf_and_matched',
             #'od_categ',
             'yfb_consignee', 'yfb_curvature_tolerance',
             #'yfb_date_first_intake_hrmonized_joined_vntu',
             'yfb_delivery_plant_tmp', 
             'yfb_dim_type_name_tmp', 'yfb_ext_corner_radius', 'yfb_grade_1', 'yfb_grade_2', 'yfb_grade_3',
             'yfb_grade_segmentation_tmp', 'yfb_heat_treatment', 'yfb_import_export_code', 'yfb_incoterm',
             'yfb_incoterm_destination', 'yfb_inspection_certificate', 'yfb_internal_grade_name', 'yfb_length_max',
             'yfb_length_min', 'yfb_length_type', "map_grade_Grade"
            #, 'yfb_mixed_grade'
            , 'yfb_ndt_type', 'yfb_pipe_end_type',
             'yfb_product_segment', 'yfb_twist_tolerance', 'yfb_ultrasound_type', 'yfb_yearly_bonus',
             'yfc_freight_provision', 'yfc_full_cost', 'yfc_full_mill_cost', 'yfc_gross_price', 'yfc_mill_fix_cost',
             'yfc_mill_var_cost', 'yfc_net_price', 'yfc_raw_material_costs',
             'ict_grade_classes_of_products', 'ict_height', 'ict_width', 'ict_od_mm', 'ict_awt_mm', 'ict_length'
             , 'ict_manufacturer_group',
             'ict_manufacturing_process_list', 'ict_mill_manufacturer_name', 'ict_qualification_list',
             'ict_source_date', 'ict_theoretical_max_length_in_m']]
             #, 'vntu_country', 'vntu_date',
             #'vntu_delta_to_market_price_indication', 'vntu_delta_to_previous_month', 'vntu_freight', 'vntu_grade',
             #'vntu_gross_price', 'vntu_items_from_rerolled_billets_ from_assel_marked_gray_in_size_range',
             #'vntu_margin_tp_based', 'vntu_margin_vlr_ltsa_based', 'vntu_od_max', 'vntu_od_min', 'vntu_sheet_name',
             #'vntu_surcharge_add_tpi', 'vntu_surcharge_for_fixed_length', 'vntu_surcharge_wt_sup_60', 'vntu_tp',
             #'vntu_vc', 'vntu_wt_max', 'vntu_wt_min',  'calc_vntu_net_price', 'calc_vntu_total_net_price',
             #'calc_vntu_total_gross_price']]
    return df
