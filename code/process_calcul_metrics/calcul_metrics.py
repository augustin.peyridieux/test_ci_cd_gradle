import sys
sys.path.append("utils/")
sys.path.append("../dependencies/utils/")
sys.path.append("pricing_project_industry-master/code/dependencies/utils/")
from sharepoint import SharepointApi
import utils
import utils_metrics
import boto3
import sys
import pandas as pd
import os

logger = utils.create_logger('PPI: Join Yellowfin ICT files')

logger.info("Starting: " + str(sys.argv[0]))

args = utils_metrics.get_parameters(logger)

s3_resource = boto3.resource('s3')
my_bucket = s3_resource.Bucket(name=args.bucket_name)

version_n = utils.get_verision_data(my_bucket, args, logger, False)
version_n_moins_un = str(float(version_n) - 1)

# utils_metrics.download_files(my_bucket, args, logger, version, version_n_moins_un)

# Déclaration des variables sharepoint
# Nom de domaine
host = os.environ['SHAREPOINT_HOST']
# Nom de la team / site / autre où les fichiers ou autres seront stockés (visible dans l'url quand dans le dossier sharepoint)
site = os.environ['SP_PPI_SITE']
# Client ID à demander à l'administrateur Office365
client_id = os.environ['PPI_CLIENT_ID']
# Client secret à demander à l'administrateur Office365
client_secret = os.environ['PPI_SP_SECRET']
# Tenant id à demander à l'administrateur Office365
tenant_id = os.environ['PPI_SP_TENANT_ID']

sharepoint_access_url = os.environ['SHAREPOINT_ACCESS_URL']

logger.info("host: " + host)
logger.info("site: " + site)
logger.info("client_id: " + client_id)
logger.info("sharepoint_access_url: " + sharepoint_access_url)

logger.info("Initialize Sharepoint client")
my_sharepointAPI = SharepointApi(host, site, client_secret, client_id, tenant_id, url_access=sharepoint_access_url)

utils_metrics.download_files(my_bucket, args, logger, version_n)

df_ict_metrics_data, df_ict_metrics_delta = utils_metrics.get_data_and_delta("ict"
                                                                , version_n, version_n_moins_un, args, logger)
utils_metrics.write_ict_file(df_ict_metrics_data, df_ict_metrics_delta, version_n, args, logger)
utils_metrics.upload_file_sharepoint(my_sharepointAPI, version_n, args, logger)

df_merged_metrics_data, df_merged_metrics_delta = utils_metrics.get_data_and_delta("merged"
                                                                , version_n, version_n_moins_un, args, logger)
utils_metrics.write_ict_file(df_ict_metrics_data, df_ict_metrics_delta, version_n, args, logger)
utils_metrics.upload_file_sharepoint(my_sharepointAPI, version_n, args, logger)
