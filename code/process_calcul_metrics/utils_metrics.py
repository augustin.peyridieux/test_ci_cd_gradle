import sys
import os
sys.path.append("utils/")
sys.path.append("../dependencies/utils/")
sys.path.append("pricing_project_industry-master/code/dependencies/utils/")
import utils
import pandas as pd
import numpy as np
from pandas.core.dtypes.common import (
    is_datetime64_any_dtype,
    is_numeric_dtype
)
import pandas as pd
import argparse


def get_parameters(logger):
    desc = "Pricing Project Industry: Calcul metrics"
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("bucket_name",
                        help="Name of the AWS S3 bucket name")
    parser.add_argument("aws_folder_intermediate",
                        help=("Name of the AWS S3 folder intermediate"))
    parser.add_argument("sharepoint_folder",
                        help=("Name of the Sharepoint folder"))
    parser.add_argument("ict_file",
                        help=("Name of the ICT file"))
    parser.add_argument("merged_file",
                        help=("Name of the merged file"))
    parser.add_argument("output_ict_file",
                        help=("Name of the ICT round file ouput"))
    parser.add_argument("output_merged_file",
                        help=("Name of the merged file output"))

    args = parser.parse_args()

    args.aws_folder_intermediate = utils.add_backslash(args.aws_folder_intermediate)
    args.sharepoint_folder = utils.add_backslash(args.sharepoint_folder)

    logger.info("bucket_name: " + str(args.bucket_name))
    logger.info("aws_folder_intermediate: " + str(args.aws_folder_intermediate))
    logger.info("sharepoint_folder: " + str(args.sharepoint_folder))
    logger.info("ict_file: " + str(args.ict_file))
    logger.info("merged_file: " + str(args.merged_file))
    logger.info("output_ict_file: " + str(args.output_ict_file))
    logger.info("output_merged_file: " + str(args.output_merged_file))
    return args


def download_files(my_bucket, args, logger, version):
    logger.info("Download AWS file: " + args.aws_folder_intermediate + version + '/' + args.ict_file)
    my_bucket.download_file(args.aws_folder_intermediate + version + '/' + args.ict_file, version + args.ict_file)

    logger.info("Download AWS file: " + args.aws_folder_intermediate + version + '/' + args.merged_file)
    my_bucket.download_file(args.aws_folder_intermediate + version + '/' + args.merged_file,
                            version + args.merged_file)
    return 0


def describe_numeric_1d(series, len_df) -> "Series":
    stat_index = (
        ["Total_records", "%_null", "nb unique values", "Average", "median", "min", "max", "sum"]
    )
    objcounts = series.value_counts()
    count_unique = len(objcounts[objcounts != 0])
    d = (
        [len_df, 1 - series.count() / len_df, count_unique, series.mean(), series.median(), series.min(), series.max(),
         series.sum()]
    )
    return pd.Series(d, index=stat_index, name=series.name)


def describe_categorical_1d(data, len_df) -> "Series":
    names = ["Total_records", "%_null", "nb unique values"]
    objcounts = data.value_counts()
    count_unique = len(objcounts[objcounts != 0])
    result = [len_df, 1 - data.count() / len_df, count_unique]
    dtype = None
    return pd.Series(result, index=names, name=data.name, dtype=dtype)


def describe_1d(data, len_df) -> "Series":
    if is_numeric_dtype(data):
        return describe_numeric_1d(data, len_df)
    else:
        return describe_categorical_1d(data, len_df)


def describe(df) -> "DataFrame":
    data = df
    len_df = len(df)
    ldesc = [describe_1d(s, len_df) for _, s in data.items()]
    # set a convenient order for rows
    names: List[Label] = []
    ldesc_indexes = sorted((x.index for x in ldesc), key=len)
    for idxnames in ldesc_indexes:
        for name in idxnames:
            if name not in names:
                names.append(name)

    d = pd.concat([x.reindex(names, copy=False) for x in ldesc], axis=1, sort=False)
    d.columns = data.columns.copy()
    return d


def calc_diff_metrics(df_metrics, version_n, version_n_moins_un):
    df_n = df_metrics[df_metrics['version'].astype(str) == version_n]
    df_n_int = df_n.dropna(how='any', axis=1).drop(columns='version')
    df_n_string = df_n[df_n.columns[df_n.isna().any()].tolist()].dropna(how='any')

    df_n_moins_un = df_metrics[df_metrics['version'].astype(str) == version_n_moins_un]
    df_n_moins_un_int = df_n_moins_un.dropna(how='any', axis=1).drop(columns='version')
    df_n_moins_un_string = df_n_moins_un[df_n_moins_un.columns[df_n_moins_un.isna().any()].tolist()].dropna(how='any')

    df_delta_string = (df_n_string - df_n_moins_un_string).add_prefix('delta_')
    df_delta_int = (df_n_int - df_n_moins_un_int).add_prefix('delta_')
    df_delta = pd.concat([df_delta_string, df_delta_int], axis=1, sort=False)
    df_delta = pd.concat([df_n, df_delta], axis=1, sort=False)
    return df_delta


def get_stats_file_merged(file, version, logger):
    logger.info("Get stats for file: " + version + ' ' + file)
    df = pd.read_csv(version + file).drop(columns='Unnamed: 0')
    for year in df['yfb_year_creation'].unique():
        df['calc_mvc_' + str(year)] = df.apply(
            lambda raw: raw['calc_mvc'] if raw['yfb_year_creation'] == int(year) else None, axis=1)
        df['yfc_gross_price_' + str(year)] = df.apply(
            lambda raw: raw['yfc_gross_price'] if raw['yfb_year_creation'] == int(year) else None, axis=1)
        df['yfc_net_price_' + str(year)] = df.apply(
            lambda raw: raw['yfc_net_price'] if raw['yfb_year_creation'] == int(year) else None, axis=1)

    year_check = 2020

    for group in ['THYSSENKRUPP', "HOBERG+DRIESCH", "SALZGITTER"]:
        df['yfc_tons_booked_per_item_' + group + str(year_check)] = df.apply(
            lambda raw: raw['yfc_tons_booked_per_item'] if (raw['map_orderer_cpy_group'] == group) & \
                                                           (raw['yfb_year_creation'] == year_check) else None, axis=1)
        df['calc_mvc_' + group + '_2020'] = df.apply(
            lambda raw: raw['calc_mvc'] if (raw['map_orderer_cpy_group'] == group) & \
                                           (raw['yfb_year_creation'] == year_check) else None, axis=1)
    for enduser in ['LIEBHERR']:
        df['yfc_tons_booked_per_item_' + enduser + str(year_check)] = df.apply(
            lambda raw: raw['yfc_tons_booked_per_item'] if (raw['map_enduser_cpy_group'] == enduser) & \
                                                           (raw['yfb_year_creation'] == year_check) else None, axis=1)
        df['calc_mvc_' + enduser + '_2020'] = df.apply(
            lambda raw: raw['calc_mvc'] if (raw['map_orderer_cpy_group'] == enduser) & \
                                           (raw['yfb_year_creation'] == year_check) else None, axis=1)

    for pipe_type in df['calc_pipe_type'].unique():
        df['yfc_tons_booked_per_item_' + pipe_type + str(year_check)] = df.apply(
            lambda raw: raw['yfc_tons_booked_per_item'] if (raw['calc_pipe_type'] == pipe_type) & \
                                                           (raw['yfb_year_creation'] == year_check) else None, axis=1)
        df['calc_mvc_' + pipe_type + str(year_check)] = df.apply(
            lambda raw: raw['calc_mvc'] if (raw['calc_pipe_type'] == pipe_type) & \
                                           (raw['yfb_year_creation'] == year_check) else None, axis=1)

    for country in ['Germany', 'France']:
        # yfb_enduser_country
        df['yfc_tons_booked_per_item_' + country + str(year_check)] = df.apply(
            lambda raw: raw['yfc_tons_booked_per_item'] if (raw['yfb_enduser_country'] == country) & \
                                                           (raw['yfb_year_creation'] == year_check) else None, axis=1)
        df['calc_mvc_' + country + str(year_check)] = df.apply(
            lambda raw: raw['calc_mvc'] if (raw['yfb_enduser_country'] == country) & \
                                           (raw['yfb_year_creation'] == year_check) else None, axis=1)
        for grade_class in ['commodity', 'fine grain qt']:
            # map_grade_class
            df['yfc_tons_booked_per_item_' + country + '_' + grade_class + str(year_check)] = df.apply(
                lambda raw: raw['yfc_tons_booked_per_item'] if (raw['yfb_enduser_country'] == country) & \
                                                               (raw['map_grade_class'] == grade_class) & \
                                                               (raw['yfb_year_creation'] == year_check) else None,
                axis=1)
            df['calc_mvc_' + country + '_' + grade_class + str(year_check)] = df.apply(
                lambda raw: raw['calc_mvc'] if (raw['yfb_enduser_country'] == country) & \
                                               (raw['map_grade_class'] == grade_class) & \
                                               (raw['yfb_year_creation'] == year_check) else None, axis=1)

    df_stats = describe(df)
    df_stats['version'] = version
    return df_stats


def get_ict_metrics(file, version, logger):
    logger.info("Get stats for file: " + version + '_' + file)
    df = pd.read_csv(version + file).drop(columns='Unnamed: 0')
    df_stats = describe(df)
    df_stats['version'] = version
    return df_stats


def get_data_and_delta(file_type, version_n, version_n_moins_un, args, logger):
    logger.info("Get data for delta")
    if file_type == 'ict':
        output_file = args.output_ict_file
        input_file = args.ict_file
    elif file_type == 'merged':
        output_file = args.merged_file
        input_file = args.output_merged_file
    else:
        raise NameError("File '" + file_type + "' is not suported. Either 'ict' or 'merged'")

    df_ict_metrics_delta = pd.read_excel(str(int(float(version_n_moins_un)))+'_'+output_file,
                                     sheet_name='data+delta', index_col=0).drop(columns='Unnamed: 0', errors='ignore')
    df_ict_metrics_data = pd.read_excel(str(int(float(version_n_moins_un)))+'_'+output_file,
                                    sheet_name='data', index_col=0).drop(columns='Unnamed: 0', errors='ignore')

    df_ict_metrics = get_ict_metrics(input_file, version_n, logger)
    df_ict_metrics_data = pd.concat([df_ict_metrics, df_ict_metrics_data], sort=False)

    df_ict_metrics = calc_diff_metrics(df_ict_metrics_data, version_n, version_n_moins_un)

    df_ict_metrics_delta = pd.concat([df_ict_metrics_delta, df_ict_metrics], sort=False)

    df_ict_metrics_data['version'] = df_ict_metrics_data['version'].astype(str)
    df_ict_metrics_delta['version'] = df_ict_metrics_delta['version'].astype(str)

    df_ict_metrics_data = df_ict_metrics_data.round(7)
    df_ict_metrics_delta = df_ict_metrics_delta.round(7)
    return df_ict_metrics_data, df_ict_metrics_delta
    

def write_ict_file(df_ict_metrics_data, df_ict_metrics_delta, version_n, args, logger):
    logger.info("Write ICT metric file")
    # Create a Pandas Excel writer using XlsxWriter as the engine.
    writer = pd.ExcelWriter(str(int(float(version_n)))+'_'+args.output_ict_file, engine='xlsxwriter')
    # Convert the dataframe to an XlsxWriter Excel object.
    df_ict_metrics_data.to_excel(writer, sheet_name='data', index_label='Headline')

    # Convert the dataframe to an XlsxWriter Excel object.
    df_ict_metrics_delta.to_excel(writer, sheet_name='data+delta', index_label='Headline')

    # Close the Pandas Excel writer and output the Excel file.
    writer.save()
    return 1


def upload_file_sharepoint(my_sharepointAPI, version_n, args, logger):
    logger.info("Upload file to sharepoint")
    # relative_url, filename, content_file
    my_sharepointAPI.upload_file(args.sharepoint_folder+"history/", str(int(float(version_n)))+'_'+args.output_ict_file)
    os.rename(str(int(float(version_n)))+'_'+args.output_ict_file,args.output_ict_file)
    # relative_url, filename, content_file
    my_sharepointAPI.upload_file(args.sharepoint_folder, args.output_ict_file)
    return 1